import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  addHours,
} from 'date-fns';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
} from 'angular-calendar';
import { Appointment } from "./models/Appointment";
import { Department } from "./models/Department";
import { Doctor } from "./models/Doctor";

export const APPS: Appointment[] = [
	{
		id: 1,
		appoint_date: '5 xxxxxxx 2564',
		appoint_time: '08:30:00',
		appoint_type: '1',
		patient: { id: 1, hn: '0000001', pname: 'Mr.', fname: 'Update', lname: 'software', right: { id: 1, right_name:  ''  } },
		refer_no: '23839-x-xxxxxxxx',
		cid: '55%',
		right: { id: 1, right_name: 'A1' },
		tel1: '080-xxxxxxx',
		doctor: 'Dr. xxxxx xxxxxxx',
		clinic: { id: 3, clinic_name: 'โรคหัวใจ' },
		diag: { id: 1, name: 'Cardiac Arrhythmia'},
	},
	{
		id: 2,
		appoint_date: '5 xxxxxxx 2564',
		appoint_time: '10:21:00',
		appoint_type: '1',
		patient: { id: 2, hn: '0000002', pname: 'Mr.', fname: 'Tim', lname: 'Wackner', right: { id: 1, right_name:  ''  } },
		refer_no: '10990-x-xxxxxxxx',
		cid: '75%',
		right: { id: 2, right_name: 'A2' },
		tel1: '060-xxxxxxx',
		doctor: 'Dr. xxxxx xxxxxxx',
		clinic: { id: 3, clinic_name: 'โรคหัวใจ' },
		diag: { id: 1, name: 'Cardiac Arrhythmia'},
	}
];

export const DOCTORS: Doctor[] = [
  {
    emp_id: 1,
    title: 'นพ.',
    employee: { id: 1, cid: '1300999999999', prefix: '', fname: 'Alexander', lname: 'Pierce'},
    
    license_no: '99999',
    license_renewal_date: '',
    tel: '080-9999999',
    depart: '',
    // specialties: ['อายุรกรรม'],
    // clinics: ['อายุกรรมทั่วไป', 'โรคหัวใจ'],
    avatar_url: 'assets/img/user2-160x160.jpg'
  },
  {
    emp_id: 2,
    title: 'พญ.',
    employee: { id: 1, cid: '1300999999999', prefix: '', fname: 'Nina', lname:'Mcintire' },
    license_no: '99999',
    license_renewal_date: '',
    tel: '080-9999999',
    depart: '',
    // specialties: ['อายุรกรรม'],
    // clinics: ['อายุกรรมทั่วไป', 'โรคหัวใจ'],
    avatar_url: 'assets/img/user4-128x128.jpg'
  },
  {
    emp_id: 3,
    title: 'พญ.',
    employee: { id: 1, cid: '1300999999999', prefix: '', fname: 'Sarah', lname: 'Bullock' },
    license_no: '99999',
    license_renewal_date: '',
    tel: '080-9999999',
    depart: '',
    // specialties: ['อายุรกรรม'],
    // clinics: ['อายุกรรมทั่วไป', 'โรคหัวใจ'],
    avatar_url: 'assets/img/user3-128x128.jpg'
  },
  {
    emp_id: 4,
    title: 'พญ.',
    employee: { id: 1, cid: '1300999999999', prefix: '', fname: 'Nadia', lname: 'Carmichael' },
    license_no: '99999',
    license_renewal_date: '',
    tel: '080-9999999',
    // position: 'นายแพทย์ชำนาญการพิเศษ',
    depart: '',
    // specialties: ['อายุรกรรม'],
    // clinics: ['อายุกรรมทั่วไป', 'โรคหัวใจ'],
    avatar_url: 'assets/img/user7-128x128.jpg'
  },
  {
    emp_id: 5,
    title: 'นพ.',
    employee: { id: 1, cid: '1300999999999', prefix: '', fname: 'Jonathan', lname: 'Burke Jr' },
    license_no: '99999',
    license_renewal_date: '',
    tel: '080-9999999',
    // position: 'นายแพทย์ชำนาญการ',
    depart: '',
    // specialties: ['อายุรกรรม'],
    // clinics: ['อายุกรรมทั่วไป', 'โรคหัวใจ'],
    avatar_url: 'assets/img/user1-128x128.jpg'
  }
];

export const EVENT_COLOR: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};

export const EVENT_ACTIONS: CalendarEventAction[] = [
  {
    label: '<i class="fas fa-fw fa-pencil-alt"></i>',
    a11yLabel: 'Edit',
    onClick: ({ event }: { event: CalendarEvent }): void => {
      // this.handleEvent('Edited', event);
      console.log('Handle event action');
      
    },
  },
  {
    label: '<i class="fas fa-fw fa-trash-alt"></i>',
    a11yLabel: 'Delete',
    onClick: ({ event }: { event: CalendarEvent }): void => {
      // this.events = this.events.filter((iEvent) => iEvent !== event);
      // this.handleEvent('Deleted', event);
      console.log('Handle event action');
    },
  },
];

export const EVENTS: CalendarEvent[] = [
  {
    id: 1,
    start: subDays(startOfDay(new Date()), 1),
    end: addDays(new Date(), 1),
    title: 'A 3 day event',
    color: EVENT_COLOR.red,
    actions: EVENT_ACTIONS,
    allDay: true,
    resizable: {
      beforeStart: true,
      afterEnd: true,
    },
    draggable: true,
  },
  {
    id: 2,
    start: startOfDay(new Date()),
    title: 'An event with no end date',
    color: EVENT_COLOR.yellow,
    actions: EVENT_ACTIONS,
  },
  {
    id: 3,
    start: subDays(endOfMonth(new Date()), 3),
    end: addDays(endOfMonth(new Date()), 3),
    title: 'A long event that spans 2 months',
    color: EVENT_COLOR.blue,
    allDay: true,
  },
  {
    id: 3,
    start: addHours(startOfDay(new Date()), 2),
    end: addHours(new Date(), 2),
    title: 'A draggable and resizable event',
    color: EVENT_COLOR.yellow,
    actions: EVENT_ACTIONS,
    resizable: {
      beforeStart: true,
      afterEnd: true,
    },
    draggable: true,
  },
  {
    id: 5,
    start: endOfMonth(new Date()),
    title: 'Test',
    color: EVENT_COLOR.yellow,
    actions: EVENT_ACTIONS,
  }
];

export const DEPARTMENTS: Department[] = [
  { id: 1, name: 'อายุรกรรม' },
  { id: 2, name: 'ศัลยกรรม' },
  { id: 3, name: 'ศัลยกรรมออร์โธปิดิกส์' },
  { id: 4, name: 'สูติกรรม' },
  { id: 5, name: 'นรีเวชกรรม' },
  { id: 6, name: 'กุมารเวชกรรม' }
];
