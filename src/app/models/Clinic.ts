export interface Clinic {
  id: number;
  clinic_name: string;
  clinic_no?: string
}
