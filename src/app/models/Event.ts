export interface Event {
    id: number;
    start: Date,
    end: Date,
    title: string,
    color: object,
    actions: [],
    allDay: boolean,
    resizable: object,
    draggable: boolean,
}