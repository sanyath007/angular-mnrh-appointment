import { Employee } from "./Employee";

export interface Doctor {
    emp_id: number;
    title: string;
    license_no: string;
    license_renewal_date: string;
    tel?: string;
    depart?: string;
    // specialties: Array<string>;
    // clinics: Array<string>;
    avatar_url?: string;
    remark?: string;
    employee: Employee;
    status?: string;
}