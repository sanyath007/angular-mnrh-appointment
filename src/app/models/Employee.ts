export interface Employee {
    id: number;
    cid?: string;
    patient_hn?: string;
    prefix: string;
    fname: string;
    lname: string;
    sex?: string;
    birthdate?: string;
    start_date?: string;
}