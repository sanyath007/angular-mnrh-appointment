export interface Hospital {
    hospcode: string;
    name: string;
    addrpart: string;
    moopart: string;
    tmbpart: string;
    amppart: string;
    chwpart: string;
    province_name: string;
}