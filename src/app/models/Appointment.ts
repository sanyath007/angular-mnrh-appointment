import { Clinic } from "./Clinic";
import { DiagGroup } from "./DiagGroup";
import { Patient } from "./Patient";
import { ReferCause } from "./ReferCause";
import { Right } from "./Right";

export interface Appointment {
    id: number;
    appoint_date: string;
    appoint_time: string;
    appoint_type: string;
    patient: Patient;
    refer_no: string;
    refer_cause?: ReferCause;
    refer_cause_text?: string;
    hospcode?: string;
    cid: string;
    right: Right;
    tel1: string;
    tel2?: string;
    doctor: any;
    clinic: Clinic;
    diag: DiagGroup;
    diag_text?: string;
    symptom?: string;
    appointer?: string;
    appointer_position?: string;
    status?: number;
}