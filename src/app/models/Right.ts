export interface Right {
    id: number;
    right_name: string;
    right_type?: number;
}
