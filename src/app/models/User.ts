export interface User {
  id: number;
  username: string;
  fullname: string;
  email: string;
  emp_code: string;
  hospcode: string;
  position: string;
  permissions?: any[];
}
