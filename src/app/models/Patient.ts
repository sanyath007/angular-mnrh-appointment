import { Right } from "./Right";

export interface Patient {
    id: number;
    hn: string;
    pname: string;
    fname: string;
    lname: string;
    cid?: string;
    sex?: number;
    birthdate?: string;
    passport?: string;
    nationality?: string;
    address?: string;
    moo?: string;
    tambon?: string;
    amphur?: string;
    changwat?: string;
    zipcode?: string;
    tel1?: string;
    tel2?: string;
    right?: Right;
    blood_group?: number;
}