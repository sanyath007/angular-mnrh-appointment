import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Appointment } from 'src/app/models/Appointment';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.css']
})
export class AppointmentsComponent implements OnInit {
  @Input() data!: Appointment[];
  @Input() appointType!: number;
  @Input() page!: number;
  @Input() pageSize!: number;
  @Output() handleDelete = new EventEmitter;
  currentUser!: any;

  constructor(private authService: AuthService) {
    this.authService.currentUser.subscribe((x: any) => this.currentUser = x?.user);
  }

  ngOnInit(): void {
  }

  onDelete(e: Event, id: number) {
    e.preventDefault();

    if (window.confirm(`คุณต้องการลบข้อมูล ID : ${id} นี่หรือไม่ ?`)) {
      this.handleDelete.emit(id);
    }
  }

  get isAdmin(): boolean {
    if (typeof this.currentUser?.permissions != 'undefined') {
      return  this.currentUser?.permissions[0]?.role?.id === 1;
    };
  
    return false;
  }

  canModify(appoint: any): boolean {
    let isOwner = this.currentUser?.hospcode === appoint.hospcode;
    let isRestrictedStatus = appoint.status === '2' || appoint.status === '3';

    if (!this.isAdmin && (!isOwner || isRestrictedStatus)) {
      return false;
    }
    
    return true;
  }
}
