import { Component, Input, OnChanges, OnInit } from '@angular/core';
import {
  FullCalendarComponent,
  CalendarOptions,
  EventClickArg,
  EventContentArg
} from '@fullcalendar/angular';

@Component({
  selector: 'app-doctor-schedule',
  templateUrl: './doctor-schedule.component.html',
  styleUrls: ['./doctor-schedule.component.css']
})
export class DoctorScheduleComponent implements OnInit, OnChanges {
  @Input() calendarEvents!: any[];
  options: CalendarOptions = {
    initialView: 'dayGridMonth',
    height: 'auto',
    buttonText: {
      today: 'วันนี้'
    },
    locale: 'th',
    displayEventTime: false,
    eventOrder: 'allDay,start',
    // datesSet: this.handleMonthChange.bind(this),
    // eventClick: this.handleEventClick.bind(this),
    // eventContent: this.handleEventRender.bind(this),
    // dateClick: this.handleDateClick.bind(this),
  };

  constructor() { }

  ngOnInit(): void {
    console.log(this.calendarEvents);
  }

  ngOnChanges(): void {
    this.options.events = this.calendarEvents;
  }
}
