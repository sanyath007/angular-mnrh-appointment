import { Component, Input, OnInit } from '@angular/core';
import { Doctor } from 'src/app/models/Doctor';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-doctor-card',
  templateUrl: './doctor-card.component.html',
  styleUrls: ['./doctor-card.component.css']
})
export class DoctorCardComponent implements OnInit {
  @Input() doctor!: any;
  currentUser!: any;

  constructor(private authService: AuthService) {
    this.authService.currentUser.subscribe(x => this.currentUser = x?.user);
  }

  ngOnInit(): void {
  }

  setCardColor(cls: number) {
    if (cls === 1) {
      return 'card-success';
    } else if (cls === 2) {
      return 'card-primary';
    } else if (cls === 3) {
      return 'card-warning';
    } else if (cls === 4) {
      return 'card-danger';
    } else {
      return 'card-success';
    }
  }

  onDelete(e: Event, id: number) {
    e.preventDefault();

    alert(`id = ${id}`);
  }
}
