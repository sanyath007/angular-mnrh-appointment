import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { HospitalService } from 'src/app/services/hospital/hospital.service';

@Component({
  selector: 'app-hospcode-list',
  templateUrl: './hospcode-list.component.html',
  styleUrls: ['./hospcode-list.component.css']
})
export class HospcodeListComponent implements OnInit {
  hospitals: any[] = [];
  filteredHospitals: any[] = [];
  page = 1;
  pageSize = 10;
  @Output() handleSelected = new EventEmitter();

  filterInputs: any = {
    keyword: '',
    condition: '1'
  };

  constructor(private hospitalService: HospitalService) { }

  ngOnInit(): void {
    this.hospitalService.getAll().subscribe((data: any) => {
      this.hospitals = data;
      this.filteredHospitals = data;
    });
  }

  onSelected(e: Event, hospcode: any) {
    e.preventDefault();

    this.handleSelected.emit(hospcode);
  }

  onFilter() {
    const { keyword, condition }: { keyword: string, condition: string } = this.filterInputs;

    this.filteredHospitals = this.hospitals.filter(hosp => {
      if (keyword !== ''){
        if (condition === '1') {
          return hosp.hospcode.includes(keyword);
        } else {
          return hosp.name.includes(keyword);
        }
      }

      return hosp;
    });
  }

  onClearFilter(): void {
    this.filterInputs.keyword = '';
    this.filterInputs.condition = '1';

    this.filteredHospitals = this.hospitals;
  }

  getHospitalType(type_id: string): string {
    const hosp_types = [
      { id: 1, name: 'สสจ.'},
      { id: 2, name: 'สสอ.'},
      { id: 3, name: 'รพ.สต.'},
      { id: 5, name: 'รพศ.'},
      { id: 7, name: 'รพท.'},
      { id: 9, name: 'ศพช.'},
      { id: 11, name: 'รพ.รัฐสังกัด สธ'},
      { id: 12, name: 'รพ.รัฐนอก สธ'},
      { id: 13, name: 'ศ.บริการนอก สธ'},
      { id: 15, name: 'รพ.เอกชน'},
    ];

    return hosp_types.find(ht => ht.id === parseInt(type_id, 10))?.name!;
  }
}
