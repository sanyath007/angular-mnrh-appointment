import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HospcodeListComponent } from './hospcode-list.component';

describe('HospcodeListComponent', () => {
  let component: HospcodeListComponent;
  let fixture: ComponentFixture<HospcodeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HospcodeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HospcodeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
