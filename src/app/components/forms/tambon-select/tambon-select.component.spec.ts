import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TambonSelectComponent } from './tambon-select.component';

describe('TambonSelectComponent', () => {
  let component: TambonSelectComponent;
  let fixture: ComponentFixture<TambonSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TambonSelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TambonSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
