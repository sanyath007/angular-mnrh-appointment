import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-select-input',
  templateUrl: './select-input.component.html',
  styleUrls: ['./select-input.component.css']
})
export class SelectInputComponent implements OnInit {
  @Input() items!: any[];
  @Input() placeholder!: string;
  @Output() onSelected = new EventEmitter();
  model: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  onChange() {
    this.onSelected.emit(this.model);
  }
}
