import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmphurSelectComponent } from './amphur-select.component';

describe('AmphurSelectComponent', () => {
  let component: AmphurSelectComponent;
  let fixture: ComponentFixture<AmphurSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmphurSelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmphurSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
