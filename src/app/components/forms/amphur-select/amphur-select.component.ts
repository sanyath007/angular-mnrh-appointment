import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-amphur-select',
  templateUrl: './amphur-select.component.html',
  styleUrls: ['./amphur-select.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: AmphurSelectComponent,
      multi: true
    }
  ]
})
export class AmphurSelectComponent implements OnInit, OnChanges, ControlValueAccessor {

  @Input() items!: any[];
  @Input() changwat: string = '';
  @Input() defaultValue: string = '';
  @Input() isInvalid: boolean = false;
  @Input() errors: any;

  disabled = false;
  value: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.defaultValue) {
      this.value = this.defaultValue;
      this.onChange(this.defaultValue);
    }

    if (this.changwat && this.items) {
      this.items = this.items.filter(item => item.chw_id == this.changwat);
    }
  }

  onItemSelected(e: Event): void {
    this.onTouched();
    this.onChange((e.target as HTMLInputElement).value);
  }

  onChange: any = () => {};
  onTouched: any = () => {};
  writeValue(value: string): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
