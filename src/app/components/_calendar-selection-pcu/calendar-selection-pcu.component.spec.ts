import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarSelectionPcuComponent } from './calendar-selection-pcu.component';

describe('CalendarSelectionPcuComponent', () => {
  let component: CalendarSelectionPcuComponent;
  let fixture: ComponentFixture<CalendarSelectionPcuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalendarSelectionPcuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarSelectionPcuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
