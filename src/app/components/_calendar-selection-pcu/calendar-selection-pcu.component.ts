import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarDateFormatter,
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView
} from 'angular-calendar';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppointmentService } from 'src/app/services/appointment/appointment.service';
import { CustomCalendarFormatter } from 'src/app/helpers/custom-calendar-formatter';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};

@Component({
  selector: 'app-calendar-selection-pcu',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './calendar-selection-pcu.component.html',
  styleUrls: ['./calendar-selection-pcu.component.css'],
  providers: [
    { provide: CalendarDateFormatter, useClass: CustomCalendarFormatter }
  ]
})
export class CalendarSelectionPcuComponent implements OnInit {
  @Output() onSelectedDate = new EventEmitter<Date>();
  // @Input() events!: CalendarEvent[];
  @ViewChild('modalContent', { static: true }) modalContent!: TemplateRef<any>;
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  viewDate: Date = new Date();
  locale: string = 'th';

  modalData!: {
    action: string;
    event: CalendarEvent;
  };

  events$!: Observable<CalendarEvent<{ app: any }>[]>;
  // actions: CalendarEventAction[] = [
  //   {
  //     label: '<i class="fas fa-fw fa-pencil-alt"></i>',
  //     a11yLabel: 'Edit',
  //     onClick: ({ event }: { event: CalendarEvent }): void => {
  //       this.handleEvent('Edited', event);
  //     },
  //   },
  //   {
  //     label: '<i class="fas fa-fw fa-trash-alt"></i>',
  //     a11yLabel: 'Delete',
  //     onClick: ({ event }: { event: CalendarEvent }): void => {
  //       this.events = this.events.filter((iEvent) => iEvent !== event);
  //       this.handleEvent('Deleted', event);
  //     },
  //   },
  // ];

  refresh: Subject<any> = new Subject();
  activeDayIsOpen: boolean = false;

  constructor(
    private modal: NgbModal,
    private appointmentService: AppointmentService
  ) { }

  ngOnInit(): void {
    this.events$ = this.appointmentService
                    .getAllByDate('2021-08-16')
                    .pipe(
                      map((results: any[]) => {
                        return results.map((app: any) => {
                          return {
                            title: `จำนวนนัด ${app.num} ราย`,
                            start: new Date(app.appoint_date),
                            color: app.num >= 10 ? colors.red : colors.blue,
                            meta: { app },
                          };
                        });
                      })
                    );
  }

  dayClicked({ date, events }: { date: Date, events: CalendarEvent[] }): void {
    if (events.length > 0) {
      const { appoint_date, num } = events[0].meta?.app;
      
      if (num >= 10) {
        alert('จำนวนนัดเต็มแล้ว');
        return;
      }
    }

    this.onSelectedDate.emit(date);

    // if (isSameMonth(date, this.viewDate)) {
    //   if (
    //     (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
    //     events.length === 0
    //   ) {
    //     this.activeDayIsOpen = false;
    //   } else {
    //     this.activeDayIsOpen = true;
    //   }

    //   this.viewDate = date;
    // }
  }

  /** On drag and drop event */
  // eventTimesChanged({ event, newStart, newEnd }: CalendarEventTimesChangedEvent): void {
  //   console.log(event);
  //   this.events = this.events.map(e => {
  //     if (e === event) {
  //       return {
  //         ...e,
  //         start: newStart,
  //         end: newEnd
  //       };
  //     }
      
  //     return e;
  //   });

  //   this.handleEvent('Dropped or resized', event);
  // }

  /** On click event */
  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { action, event };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }
}
