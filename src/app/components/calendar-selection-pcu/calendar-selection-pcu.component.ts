import {
  Component,
  EventEmitter,
  OnChanges,
  OnInit,
  Input,
  Output,
  ViewChild, 
  TemplateRef
} from '@angular/core';
import {
  FullCalendarComponent,
  CalendarOptions,
  EventClickArg,
  EventContentArg
} from '@fullcalendar/angular';
import { DateClickArg } from '@fullcalendar/interaction';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { AppointmentService } from 'src/app/services/appointment/appointment.service';

@Component({
  selector: 'app-calendar-selection-pcu',
  templateUrl: './calendar-selection-pcu.component.html',
  styleUrls: ['./calendar-selection-pcu.component.css']
})
export class CalendarSelectionPcuComponent implements OnInit, OnChanges {
  @ViewChild('modalEvent') modalEvent!: TemplateRef<any>;
  @ViewChild('calendar') calendar!: FullCalendarComponent;
  @Output() onMonthChange = new EventEmitter();
  @Output() onSelectedDate = new EventEmitter();
  @Input() clinicSelected!: string;
  // @Input() events!: any[];
  // events!: any[];

  modalData!: {
    date: string;
    admits: any[];
  };

  options: CalendarOptions = {
    initialView: 'dayGridMonth',
    height: 'auto',
    buttonText: {
      today: 'วันนี้'
    },
    locale: 'th',
    displayEventTime: false,
    eventOrder: 'allDay,start',
    events: [],
    datesSet: this.handleMonthChange.bind(this),
    eventClick: this.handleEventClick.bind(this),
    eventContent: this.handleEventRender.bind(this),
    dateClick: this.handleDateClick.bind(this),
  };

  constructor(
    private toastr: ToastrService,
    private modalService: NgbModal,
    private appointmentService: AppointmentService
  ) { }

  ngOnInit(): void {
    this.appointmentService
      .getAllByClinic(moment().format('YYYY-MM-DD'), 1, this.clinicSelected)
      .subscribe((data: any) => {
        const { appointments, postponements } = data;

        const app_events = appointments.map((app: any) => {
                          return {
                            title: `จำนวนนัด ${app.num} ราย`,
                            start: app.appoint_date,
                            backgroundColor: this.clinicSelected == '10' ? '#266A2E' : this.clinicSelected == '11' ? '#e42c64' : '#3a9ad9',
                            extendedProps: {
                              clinic: this.clinicSelected,
                              count: app.num
                            },
                            isPostponed: false
                          };
                        });

        /** Append postponements data */
        const post_events = postponements.map((post: any) => {
            return {
              title: `เลื่อน ${post.amt} ราย`,
              start: post.old_date,
              backgroundColor: '#716e77',
              isPostponed: true
            }
        });

        this.options.events = [ ...app_events, ...post_events ];
      });

      // this.appointmentService
      //   .getAllByDate(moment().format('YYYY-MM-DD'), 1)
      //   .subscribe(data => {
      //     this.options.events = data.map((app: any) => {
      //                             return {
      //                               title: `จำนวนนัด ${app.num} ราย`,
      //                               start: app.appoint_date,
      //                               backgroundColor: app.num >= 10 ? '#1f640a' : '#e42c64'
      //                             };
      //                           });
      //   });
  }

  ngOnChanges(): void {
  }

  /** On change month on toolbar */
  handleMonthChange(arg: any) {
    console.log('onMonthChange...', arg);
    
    const currentDate = arg.view?.calendar?.currentData?.currentDate;
    
    // this.onMonthChange.emit(moment(currentDate).format('YYYY-MM-DD'));
  }

  handleEventClick(arg: EventClickArg) {
    console.log('onEventClick...', arg);
    
    // if (arg.event?.title === '12') {
    //   this.toastrService.error('ไม่พบรายการนัดประจำวันนี้ !!', 'ผลการตรวจสอบ');
    //   return;
    // }

    // let date = moment(arg.event?.start).format('YYYY-MM-DD');

    // /** Fetch appointments data that admit between event start date and set to admits element */
    // this.appointmentService.getAll().subscribe((data: any[]) => {
    //   /** Filter appointment data if selected date is between admdate and dchdate  */
    //   let admits = data.filter(app => moment(date).isSameOrAfter(app.admdate) && moment(date).isSameOrBefore(app.dchdate));
    //   /** Set modal content */
    //   this.modalData = { date, admits };
    //   /** Show modal popup */
    //   this.modalService.open(this.modalEvent, { ariaLabelledBy: 'modal-basic-title', size: 'xl' });
    // });
  }

  /** Set custom rendering event */
  handleEventRender(info: EventContentArg) {
    console.log('onEventRender...', info);
    
    // const event = info.event;

    // if (info.event.display === 'list-item') {
    //   return { html: '<i class="fas fa-star text-secondary"></i>' }
    // } else {
    //   return { html: `<div>${event.title}</div>` };
    // }
  }

  /** On date is selected */
  handleDateClick(arg: DateClickArg) {
    console.log('onDateClick...', arg);
    if ([0,6].includes(moment(arg.date).weekday())) {
      this.toastr.error('ไม่สามารถเลือกวันเสาร์-อาทิตย์ได้!!', 'ผลการตรวจสอบ');
      return;
    }

    if (moment(arg.date).isBefore(moment(), "date")) {
      this.toastr.error('ไม่สามารถเลือกวันย้อนหลังได้!!', 'ผลการตรวจสอบ');
      return;
    }

    let event: any;
    const { events } = this.options;
    if (Array.isArray(events)) {
      event = events.find((ev: any) => {
        return !ev.isPostponed && moment(ev.start).format('YYYY-MM-DD') === moment(arg.date).format('YYYY-MM-DD');
      });
    }

    if (event) {
      const { clinic, count } = event.extendedProps;

      if (
        (clinic == '10' && parseInt(count, 10) + 1 > 10) ||
        (clinic == '11' && parseInt(count, 10) + 1 > 10) ||
        (clinic == '12' && parseInt(count, 10) + 1 > 5) ||
        (clinic == '13' && parseInt(count, 10) + 1 > 5)
      ) {
        this.toastr.error('คิวเต็มแล้ว!!', 'ผลการตรวจสอบ');
      } else {
        this.onSelectedDate.emit(arg.date);
      }
    } else {
      this.onSelectedDate.emit(arg.date);
    }
  }
}
