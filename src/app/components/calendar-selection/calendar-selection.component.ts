import {
  Component,
  EventEmitter,
  OnInit,
  Input,
  Output,
  ViewChild, 
  TemplateRef
} from '@angular/core';
import {
  FullCalendarComponent,
  CalendarOptions,
  EventClickArg,
  EventContentArg
} from '@fullcalendar/angular';
import { DateClickArg } from '@fullcalendar/interaction';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { AppointmentService } from 'src/app/services/appointment/appointment.service';
import { DoctorService } from 'src/app/services/doctor/doctor.service';

@Component({
  selector: 'app-calendar-selection',
  templateUrl: './calendar-selection.component.html',
  styleUrls: ['./calendar-selection.component.css']
})
export class CalendarSelectionComponent implements OnInit {
  @ViewChild('modalEvent') modalEvent!: TemplateRef<any>;
  @ViewChild('calendar') calendar!: FullCalendarComponent;
  @Output() onMonthChange = new EventEmitter();
  @Output() onSelectedDate = new EventEmitter();
  @Input() doctorSelected!: string;
  events!: any[];

  modalData!: {
    date: string;
    admits: any[];
  };

  options: CalendarOptions = {
    initialView: 'dayGridMonth',
    height: 'auto',
    buttonText: {
      today: 'วันนี้'
    },
    locale: 'th',
    displayEventTime: false,
    eventOrder: 'allDay,start',
    datesSet: this.handleMonthChange.bind(this),
    eventClick: this.handleEventClick.bind(this),
    eventContent: this.handleEventRender.bind(this),
    dateClick: this.handleDateClick.bind(this),
  };

  constructor(
    private modalService: NgbModal,
    private toastrService: ToastrService,
    private appointmentService: AppointmentService,
    private doctorService: DoctorService
  ) { }

  ngOnInit(): void {
    this.doctorService
      .getDoctorSchedules(this.doctorSelected)
      .subscribe((data: any[]) => {
        this.options.events = this.createEventFromSchedules(data);
      });
  }

  createEventFromSchedules(schedules: any[]): any[] {
    let events: any[] = [];
    let scheduleDays!: any[];

    schedules.forEach((sch: any) => {
      let periodText = '';
      const days = sch.days.split(',');

      scheduleDays = days.map((d: any) => `${sch.month}-${d}`);
      periodText = sch.period === '1'
                    ? '08.00 - 12.00 น.'
                    : sch.period === '2' ? '13.00 - 16.00 น.' : '16.00 - 20.00 น.'

      let i = 0;

      do {
        let d = moment(scheduleDays[i]);
        i++;

        events.push({
          title: periodText,
          start: d.format('YYYY-MM-DD'),
          extendedProps: {
            date: d.format('YYYY-MM-DD'),
            period: sch.period
          },
          allDay: true,
          backgroundColor: sch.period === '1' ? '#8bc24c' : sch.period === '2' ? '#5bd1d7' : '#f5587b'
        });
      } while(i < scheduleDays.length);
    });

    return events;
  }

  /** On change month on toolbar */
  handleMonthChange(arg: any) {
    const currentDate = arg.view?.calendar?.currentData?.currentDate;
    
    // this.onMonthChange.emit(moment(currentDate).format('YYYY-MM-DD'));
  }

  handleEventClick(arg: EventClickArg) {
    // if (arg.event?.title === '12') {
    //   this.toastrService.error('ไม่พบรายการนัดประจำวันนี้ !!', 'ผลการตรวจสอบ');
    //   return;
    // }

    let data = { date: moment(arg.event?.start).toDate(), period: arg.event.extendedProps.period };

    this.onSelectedDate.emit(data);

    // /** Fetch appointments data that admit between event start date and set to admits element */
    // this.appointmentService.getAll().subscribe((data: any[]) => {
    //   /** Filter appointment data if selected date is between admdate and dchdate  */
    //   let admits = data.filter(app => moment(date).isSameOrAfter(app.admdate) && moment(date).isSameOrBefore(app.dchdate));
    //   /** Set modal content */
    //   this.modalData = { date, admits };
    //   /** Show modal popup */
    //   this.modalService.open(this.modalEvent, { ariaLabelledBy: 'modal-basic-title', size: 'xl' });
    // });
  }

  /** Set custom rendering event */
  handleEventRender(info: EventContentArg) {
    console.log(info);
    
    // const event = info.event;

    // if (info.event.display === 'list-item') {
    //   return { html: '<i class="fas fa-star text-secondary"></i>' }
    // } else {
    //   return { html: `<div>${event.title}</div>` };
    // }
  }

  /** On date is selected */
  handleDateClick(arg: DateClickArg) {
    console.log(arg);

    this.onSelectedDate.emit(arg.date);
  }
}
