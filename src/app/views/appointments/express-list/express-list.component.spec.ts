import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentExpressListComponent } from './express-list.component';

describe('AppointmentExpressListComponent', () => {
  let component: AppointmentExpressListComponent;
  let fixture: ComponentFixture<AppointmentExpressListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppointmentExpressListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentExpressListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
