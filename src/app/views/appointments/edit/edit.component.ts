import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ModalDismissReasons,
  NgbModal,
  NgbDateStruct,
  NgbInputDatepicker,
} from '@ng-bootstrap/ng-bootstrap';
// import { createMask } from '@ngneat/input-mask';
import Stepper from 'bs-stepper';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { Formatter } from 'src/app/helpers/formatter';
import { AppointmentService } from 'src/app/services/appointment/appointment.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { PatientService } from 'src/app/services/patient/patient.service';

@Component({
  selector: 'app-appointment-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class AppointmentEditComponent implements OnInit {
  private stepper!: Stepper;
  isLinear!: false;
  appointmentForm!: FormGroup;
  closeModal!: string;

  /** Initial data for dropdown controls */
  clinics!: any[];
  diagGroups!: any[];
  referCauses!: any[];
  rights!: any[];
  doctors!: any[];
  rooms!: any[];

  dateModel!: NgbDateStruct;
  @ViewChild('birthDate') birthDate!: NgbInputDatepicker;

  appointType!: number;
  submitted: boolean = false;
  currentUser!: any;

  /** Input mask formating */
  // appointTimeInputMask = createMask<string>({
  //   alias: 'datetime',
  //   inputFormat: 'HH:MM'
  // });

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private modalService: NgbModal,
    private formatter: Formatter,
    private appointmentService: AppointmentService,
    private patientService: PatientService,
    private authService: AuthService
  ) {
    this.authService.currentUser.subscribe(x => this.currentUser = x?.user);
  }

  ngOnInit(): void {
    let id: string = '';
    /** Get route params and set to appointType */
    this.route.params.subscribe(params => {
      this.appointType = parseInt(params.type);
      id = params.id;
    });

    this.stepper = new Stepper(document.querySelector('#stepper')!, {
      linear: false,
      animation: true
    });

    this.appointmentForm = this.fb.group({
      id: [''],
      appoint_date: ['', Validators.required],
      appoint_time: [this.appointType === 1 ? '1' : '', Validators.required],
      appoint_type: [this.appointType === 1 ? '1' : '2'],
      clinic: [this.appointType === 1 ? '9' : '', Validators.required],
      doctor: [''],
      room: [this.appointType === 1 ? '5' : ''],
      diag_group: [this.appointType === 1 ? '999' : '', Validators.required],
      diag_text: [''],
      symptom: [''],
      refer_no: [this.appointType === 1 ? 'pcu/รพ.สต.' : '', Validators.required],
      refer_cause: [this.appointType === 1 ? '99' : '', Validators.required],
      refer_cause_text: [''],
      patient_id: [''],
      patient_hn: [''],
      cid: ['', Validators.required],
      passport: [''],
      pname: ['', Validators.required],
      fname: ['', Validators.required],
      lname: ['', Validators.required],
      sex: [''],
      birthdate: [''],
      tel1: ['', Validators.required],
      tel2: [''],
      patient_right: ['', Validators.required],
      hospcode: [this.currentUser ? this.currentUser.hospcode : '' ],
      appointer: ['', Validators.required],
      appointer_position: ['', Validators.required],
      user: [this.currentUser ? this.currentUser.user_id : '' ],
    });

    this.appointmentService.getInitForm().subscribe(res => {
      this.clinics = res.clinics.filter((c: any) => [10,11,12,13].includes(c.id));
      this.diagGroups = res.diagGroups;
      this.referCauses = res.referCauses;
      this.rights = this.appointType === 1 ? res.rights.filter((r: any) => r.id == '6') : res.rights;
      this.doctors = res.doctors;
      this.rooms = res.rooms;
    });

    this.appointmentService.getById(id).subscribe(data => {
      let appointDate = moment(data.appoint_date).toDate();

      this.appointType = parseInt(data.appoint_type, 10);
      this.appointmentForm.controls['id'].setValue(data.id);
      this.appointmentForm.controls['clinic'].setValue(data.clinic.id);
      this.appointmentForm.controls['diag_group'].setValue(data.diag ? data.diag.id : '999');
      this.appointmentForm.controls['diag_text'].setValue(data.diag_text);
      this.appointmentForm.controls['symptom'].setValue(data.symptom);
      this.appointmentForm.controls['refer_no'].setValue(data.refer_no);
      this.appointmentForm.controls['refer_cause'].setValue(data.refer_cause?.id);
      this.appointmentForm.controls['refer_cause_text'].setValue(data.refer_cause_text);
      this.appointmentForm.controls['appoint_date'].setValue(`${appointDate.getDate()}/${appointDate.getMonth()+1}/${appointDate.getFullYear()+543}`);
      this.appointmentForm.controls['appoint_time'].setValue(data.appoint_time);
      this.appointmentForm.controls['appointer'].setValue(data.appointer);
      this.appointmentForm.controls['appointer_position'].setValue(data.appointer_position);

      this.appointmentForm.controls['patient_id'].setValue(data.patient.id);
      this.appointmentForm.controls['patient_hn'].setValue(data.patient.hn);
      this.appointmentForm.controls['cid'].setValue(data.patient.cid);
      data.patient.passport && this.appointmentForm.controls['passport'].setValue(data.patient.passport);
      this.appointmentForm.controls['pname'].setValue(data.patient.pname);
      this.appointmentForm.controls['fname'].setValue(data.patient.fname);
      this.appointmentForm.controls['lname'].setValue(data.patient.lname);
      this.appointmentForm.controls['sex'].setValue(data.patient.sex);
      this.appointmentForm.controls['birthdate'].setValue(this.formatter.dbdateToThdate(data.patient.birthdate!));
      this.appointmentForm.controls['tel1'].setValue(data.patient.tel1);
      data.patient.tel2 && this.appointmentForm.controls['tel2'].setValue(data.patient.tel2);
      this.appointmentForm.controls['patient_right'].setValue(data.right.id);
    });
  }

  next(e: Event) {
    e.preventDefault();
  
    this.stepper.next();
  }

  onSubmit() {
    this.submitted = true;
    
    if (this.appointmentForm.invalid) {
      /** Set stepper to first step */
      this.stepper.to(1);
      return;
    }

    const id = this.appointmentForm.controls['id'].value;

    this.appointmentService.update(id, this.appointmentForm.value).subscribe(data => {
      this.toastr.success('แก้ไขข้อมูลเรียบร้อย!!', 'ผลการทำงาน');

      if (this.appointType === 1) {
        this.router.navigate(['/appointments-pcu']);
      } else {
        this.router.navigate(['/appointments']);
      }
    });
  }

  get f() { return this.appointmentForm.controls; }

  onSearchPatientByCid(event: Event, cid: string) {
    // TODO: get patient data by cid if exists in db
    event.preventDefault();

    if (cid === '' || cid.length < 13) {
      return;
    }

    if (cid.length === 13) {
      console.log(cid);
      this.patientService.getByCid(cid).subscribe(data => {
        if (data) this.setPatientValue(data)
      });
    }
  }

  onShowPatientModal(content: any) {
    /** Select patient from modal popup */
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' });
  }

  triggerModal(content: any) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' })
      .result
      .then(res => {
        this.closeModal = `Close with: ${res}`;
      }, res => {
        this.closeModal = `Dismissed ...`
      });
  }

  /** Calendar */
  onAppointed(date: Date, modal: any) {
    /** Set date to control input */
    this.appointmentForm.controls['appoint_date'].setValue(moment(date).format('DD/MM/YYYY'));

    /** ปิดหน้าต่าง Popup */
    modal.close();
  }

  /** Datepicker */
  onDateSelected() {
    console.log('on date selected');
    this.birthDate.toggle();
  }

  setPatientValue(patient: any): void {
    this.appointmentForm.controls['patient_id'].setValue(patient.id);
    this.appointmentForm.controls['patient_hn'].setValue(patient.hn);
    this.appointmentForm.controls['cid'].setValue(patient.cid);
    this.appointmentForm.controls['passport'].setValue(patient.passport);
    this.appointmentForm.controls['pname'].setValue(patient.pname);
    this.appointmentForm.controls['fname'].setValue(patient.fname);
    this.appointmentForm.controls['lname'].setValue(patient.lname);
    this.appointmentForm.controls['sex'].setValue(patient.sex);
    this.appointmentForm.controls['birthdate'].setValue(moment(patient.birthdate).format('DD/MM/YYYY'));
    this.appointmentForm.controls['tel1'].setValue(patient.tel1);
    this.appointmentForm.controls['tel2'].setValue(patient.tel2);
    this.appointmentForm.controls['patient_right'].setValue(patient.main_right);
  }

  onPatientSelected(patient: any, modal: any) {
    /** Set ข้อมูลจาก Popup to form controls */
    this.setPatientValue(patient);
    
    /** ปิดหน้าต่าง Popup */
    modal.close();
  }
}
