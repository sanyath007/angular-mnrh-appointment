import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ModalDismissReasons,
  NgbModal,
  NgbDateStruct,
  NgbInputDatepicker,
} from '@ng-bootstrap/ng-bootstrap';
// import { createMask } from '@ngneat/input-mask';
import Stepper from 'bs-stepper';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { Right } from 'src/app/models/Right';
import { AppointmentService } from 'src/app/services/appointment/appointment.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { DoctorService } from 'src/app/services/doctor/doctor.service';
import { PatientService } from 'src/app/services/patient/patient.service';

@Component({
  selector: 'app-appointment-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AppointmentAddComponent implements OnInit {
  private stepper!: Stepper;
  isLinear!: false;
  appointmentForm!: FormGroup;
  closeModal!: string;

  /** Initial data for dropdown controls */
  clinics!: any[];
  tmpDiagGroups!: any[];
  diagGroups!: any[];
  referCauses!: any[];
  rights!: any[];
  doctors!: any[];
  rooms!: any[];

  dateModel!: NgbDateStruct;
  @ViewChild('birthDate') birthDate!: NgbInputDatepicker;

  appointType!: number;
  submitted = false;
  loading = false;
  currentUser!: any;

  /** Input mask formating */
  // appointTimeInputMask = createMask<string>({
  //   alias: 'datetime',
  //   inputFormat: 'HH:MM'
  // });

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private toastr: ToastrService,
    private appointmentService: AppointmentService,
    private patientService: PatientService,
    private doctorService: DoctorService,
    private authService: AuthService
  ) {
    this.authService.currentUser.subscribe(x => this.currentUser = x?.user);
  }

  ngOnInit(): void {
    /** Get route params and set to appointType */
    this.route.params.subscribe(params => this.appointType = parseInt(params.type));

    this.stepper = new Stepper(document.querySelector('#stepper')!, {
      linear: false,
      animation: true
    });

    this.appointmentForm = this.fb.group({
      appoint_date: ['', Validators.required],
      appoint_time: [this.appointType === 1 ? '1' : '', Validators.required],
      appoint_type: [this.appointType === 1 ? '1' : '2'],
      clinic: ['', Validators.required],
      doctor: [''],
      room: [this.appointType === 1 ? '5' : ''],
      diag_group: [this.appointType === 1 ? '999' : '', Validators.required],
      diag_text: [''],
      symptom: [''],
      refer_no: [this.appointType === 1 ? 'pcu/รพ.สต.' : '', Validators.required],
      refer_cause: ['', Validators.required],
      refer_cause_text: [''],
      patient_id: [''],
      patient_hn: [''],
      cid: ['', Validators.required],
      passport: [''],
      pname: ['', Validators.required],
      fname: ['', Validators.required],
      lname: ['', Validators.required],
      sex: [''],
      birthdate: [''],
      tel1: ['', Validators.required],
      tel2: [''],
      patient_right: ['', Validators.required],
      hospcode: [this.currentUser ? this.currentUser.hospcode : '' ],
      appointer: [this.currentUser ? this.currentUser.fullname : '', Validators.required],
      appointer_position: [this.currentUser ? this.currentUser.position : '', Validators.required],
      user: [this.currentUser ? this.currentUser.user_id : '' ],
    });

    this.appointmentService.getInitForm().subscribe(res => {
      this.clinics = res.clinics.filter((clinic: any) => parseInt(clinic.is_special, 10) === 1);
      this.tmpDiagGroups = res.diagGroups;
      this.referCauses = res.referCauses.filter((rc: any) => [4,5,99].includes(rc.id));
      this.rights = this.appointType === 1 ? res.rights.filter((r: any) => r.id == '6') : res.rights;8
      this.doctors = res.doctors;
      this.rooms = res.rooms;

      if (this.appointType === 1) {
        this.diagGroups = res.diagGroups;
      }
    });
  }

  next(e: Event) {
    e.preventDefault();
  
    this.stepper.next();
  }

  onSubmit() {
    this.submitted = true;

    if (this.appointmentForm.invalid) {
      /** Set stepper to first step */
      this.stepper.to(1);

      this.toastr.warning('คุณกรอกข้อมูลไม่ครบ!!', 'ผลการตรวจสอบ');

      return;
    }

    this.appointmentService.store(this.appointmentForm.value).subscribe(data => {
      this.toastr.success('บันทึกข้อมูลเรียบร้อย!!', 'ผลการทำงาน');

      if (this.appointType === 1) {
        this.router.navigate(['/appointments-pcu']);
      } else {
        this.router.navigate(['/appointments']);
      }
    });
  }

  get f() { return this.appointmentForm.controls; }

  onSearchPatientByCid(event: Event, cid: string) {
    // TODO: get patient data by cid if exists in db
    event.preventDefault();

    if (cid === '' || cid.length < 13) {
      return;
    }

    if (cid.length === 13) {
      this.patientService.getByCid(cid).subscribe(data => {
        if (data) this.setPatientValue(data)
      });
    }
  }

  onSearchPatientByHn(event: Event, hn: string) {
    /** Get patient data by cid if exists in db (test hn=871496) */
    event.preventDefault();

    if (hn === '') {
      this.toastr.warning('กรุณากรอก HN ก่อน !!', 'ผลการตรวจสอบ');
      return;
    }

    this.loading = true;

    /** Fetch patient data from HOMC */
    this.patientService.getHByHn(hn).subscribe((data: any) => {
      if (data) {
        if (data.cid && data.cid.length === 13) {
          /** Check cid from appointment db */
          this.patientService.getByCid(data.cid).subscribe(res => {
            if (res) {
              this.setPatientValue(res);
            } else {
              this.setPatientValue(data);
            }

            this.loading = false;
          });
        }
      } else {
        this.loading = false;
        this.toastr.error('ไม่ข้อมูลผู้ป่วย !!', 'ผลการทำงาน')
      }
    });
  }

  onShowPatientModal(content: any) {
    /** Select patient from modal popup */
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' });
  }

  triggerModal(content: any) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' })
      .result
      .then(res => {
        this.closeModal = `Close with: ${res}`;
      }, res => {
        this.closeModal = `Dismissed ...`
      });
  }

  showCalendar(content: any, clinic: string) {
    if (clinic == '') {
      this.toastr.error('กรุณาเลือกคลินิกก่อน !!', 'ผลการตรวจสอบ')
      return;
    }

    this.triggerModal(content);
  }

  /** Handling on user select date on calendar of PCU */
  onAppointed(date: Date, modal: any) {
    /** Set date to control input */
    this.appointmentForm.controls['appoint_date'].setValue(`${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()+543}`);

    /** ปิดหน้าต่าง Popup */
    modal.close();
  }

  /** Handling on user select date on calendar */
  handleAppointed(data: any, modal: any) {
    const { date, period } = data;
    /** Set data's element value to input controls */
    this.appointmentForm.controls['appoint_date'].setValue(`${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()+543}`);
    this.appointmentForm.controls['appoint_time'].setValue(period);

    /** ปิดหน้าต่าง Popup */
    modal.close();
  }

  setPatientValue(patient: any): void {
    let right: string;

    if (patient.useDrg) {
      const rt = this.mapRight(patient.useDrg);
      right  = rt ? rt.id.toString() : '';
    } else if (patient.main_right) {
      right = patient.main_right;
    } else {
      right = '';
    }

    this.appointmentForm.controls['patient_id'].setValue(patient.id);
    this.appointmentForm.controls['patient_hn'].setValue(patient.hn);
    this.appointmentForm.controls['cid'].setValue(patient.cid);
    this.appointmentForm.controls['passport'].setValue(patient.passport ? patient.passport : '');
    this.appointmentForm.controls['pname'].setValue(patient.pname);
    this.appointmentForm.controls['fname'].setValue(patient.fname);
    this.appointmentForm.controls['lname'].setValue(patient.lname);
    this.appointmentForm.controls['sex'].setValue(patient.sex);
    this.appointmentForm.controls['birthdate'].setValue(this.toThdate(patient.birthdate));
    this.appointmentForm.controls['tel1'].setValue(patient.tel1);
    this.appointmentForm.controls['tel2'].setValue(patient.tel2 ? patient.tel2 : '');
    this.appointmentForm.controls['patient_right'].setValue(right);
  }

  // TODO: to separate this method to another module because it have been used in both add and edit component
  mapRight(useDrg: string): Right | null {
    if (!useDrg) return '' || null;

    let right = this.rights.find(rt => rt.use_drg === useDrg);

    return right;
  }

  toThdate(str: string): string {
    let [year, month, day] = str.split('-');

    return `${day}/${month}/${parseInt(year, 10) + 543}`;
  }

  onPatientSelected(patient: any, modal: any) {
    /** Set ข้อมูลจาก Popup to form controls */
    this.setPatientValue(patient);
    
    /** ปิดหน้าต่าง Popup */
    modal.close();
  }

  onSelectedClinic(clinicId: number) {
    this.diagGroups = this.tmpDiagGroups.filter(dg => dg.clinic === clinicId);
    // TODO: To get doctor and room data according to clinic that user is selected
    let clinic  = this.clinics.find(cl => cl.id == clinicId);

    this.appointmentForm.controls['room'].setValue(clinic.room_id);
    this.appointmentForm.controls['appoint_date'].setValue('');
    
    if (this.appointType !== 1) {
      this.doctorService.getDortorsOfClinic(parseInt(clinic.specialist, 10)).subscribe(data => {
        this.appointmentForm.controls['doctor'].setValue(data.emp_id);      
      });
    }
  }
}
