import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// import jsPDF from 'jspdf';
// import html2canvas from 'html2canvas';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';
import { Appointment } from 'src/app/models/Appointment';
import { AppointmentService } from 'src/app/services/appointment/appointment.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-appointment-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class AppointmentDetailComponent implements OnInit {
  @ViewChild('htmlData') htmlData!:ElementRef;
  appointment!: Appointment;
  apiUrl!: string;
  currentUser!: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private appointmentService: AppointmentService,
    private authService: AuthService
  ) {
    this.authService.currentUser.subscribe((x: any) => this.currentUser = x?.user);
  }

  ngOnInit(): void {
    let id: string = '';
    this.route.params.subscribe(param => id = param.id);

    this.appointmentService.getById(id).subscribe(data => this.appointment = data);

    this.apiUrl = environment.apiUrl;
  }

  calcAge(birthdate: Date | any): number {
    if (!birthdate) return 0;

    return moment().diff(moment(birthdate), 'years');
  }

  onComplete(event: Event, id?: number, appointType?: number) {
    event.preventDefault();
    console.log(id);

    this.appointmentService.complete(id!, {}).subscribe(data => {
      if (data.status === 1) {
        this.toastr.success('บันทึกข้อมูลเรียบร้อย !!', 'ผลการทำงาน');

        if (appointType === 1) {
          this.router.navigate(['/appointments-pcu']);
        } else {
          this.router.navigate(['/appointments']);
        }
      } else {
        this.toastr.error('พบข้อมูลผิดพลาด !!', 'ผลการตรวจสอบ');
      }
    });
  }

  onCancel(event: Event, id?: number, appointType?: number) {
    event.preventDefault();
    console.log(id);

    this.appointmentService.cancel(id!, {}).subscribe(data => {
      if (data.status === 1) {
        this.toastr.success('ยกเลิกข้อมูลเรียบร้อย !!', 'ผลการทำงาน');

        if (appointType === 1) {
          this.router.navigate(['/appointments-pcu']);
        } else {
          this.router.navigate(['/appointments']);
        }
      } else {
        this.toastr.error('พบข้อมูลผิดพลาด !!', 'ผลการตรวจสอบ');
      }
    });
  }

  canModify(appoint: any): boolean {
    if (appoint) {
      let isOwner = this.currentUser.hospcode === appoint.hospcode;
      let isRestrictedStatus = appoint.status === '2' || appoint.status === '3';
  
      if (!this.isAdmin && (!isOwner || isRestrictedStatus)) {
        return false;
      }
    }
    
    return true;
  }

  get isAdmin(): boolean {
    if (typeof this.currentUser?.permissions != 'undefined') {
      return  this.currentUser?.permissions[0]?.role?.id === 1;
    };
  
    return false;
  }

  get isOfficer(): boolean { 
    if (typeof this.currentUser?.permissions != 'undefined') {
      return  this.currentUser?.permissions[0]?.role?.id === 4;
    };
  
    return false;
  }

  get isPcu() {
    if (typeof this.currentUser?.permissions != 'undefined') {
      return  this.currentUser?.permissions[0]?.role?.id === 5;
    };
  
    return false;
  }

  openPdf(e: Event): void {
    e.preventDefault();

    /** get html element by using ElementRef */
    // let data = this.htmlData.nativeElement;

    /** get html element by using document */
    // let data = document.getElementById('htmlData');

    /** Create pdf from html */
    // let doc = new jsPDF('p', 'pt', 'a4');
    // doc.html(data!, {
    //   html2canvas: {
    //     width: 200
    //   },
    //   callback: function () {
    //     doc.output('dataurlnewwindow');
    //   }
    // });

    /** Create pdf from image */
    // html2canvas(data)
    //   .then(canvas => {
    //     var imgW = 210;
    //     var imgH = canvas.height * imgW / canvas.width;
    //     const contentDataUrl = canvas.toDataURL('image/png');
    //     let doc = new jsPDF('p', 'mm', 'a4');
    //     var position = 20;
    //     doc.addImage(contentDataUrl, 'PNG', 10, position, imgW, imgH);
    //     doc.save('newPDF.pdf');
    //   });
  }
}
