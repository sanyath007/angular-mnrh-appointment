import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { first } from 'rxjs/operators';
import * as moment from 'moment';
import { AppointmentService } from 'src/app/services/appointment/appointment.service';
import { Appointment } from 'src/app/models/Appointment';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-appointment-pcu-list',
  templateUrl: './pcu-list.component.html',
  styleUrls: ['./pcu-list.component.css'],
})
export class AppointmentPcuListComponent implements OnInit {
  page = 1;
  pageSize = 10;
  appointments: Appointment[] = [];
  filteredAppointments: Appointment[] = [];
  
  filterInputs: any = {
    keyword: '',
    condition: '2',
    appointDate: ''
  };

  constructor(
    private toastr: ToastrService,
    private appointmentService: AppointmentService
  ) { }

  ngOnInit(): void {
    this.appointmentService
      .getAll()
      .pipe(first())
      .subscribe(app => {
        this.appointments = app.filter(a => a?.appoint_type === '1');
        this.filteredAppointments = app.filter(a => a?.appoint_type === '1');
      })
  }

  onClearFilter(): void {
    this.filterInputs.keyword = '';
    this.filterInputs.condition = '1';
    this.filterInputs.admDate = '';

    this.filteredAppointments = this.appointments;
  }

  onFilter() {
    const { keyword, condition, appointDate } = this.filterInputs;

    this.filteredAppointments = this.appointments.filter(app => {
      if (keyword !== ''){
        console.log(condition);
        
        if (condition === '1') {
          return app.refer_no.includes(keyword);
        } else if (condition === '2') {
          return app.patient.cid === keyword;
        } else if (condition === '3') {
          return app.patient.fname.includes(keyword);
        }
      }

      return app;
    }).filter(app => {
      if (appointDate !== '') {
        return app.appoint_date === this.formatDate(appointDate);
      }

      return app;
    });
  }

  formatDate(date: string): string {
    const [d, m, y] = date.split('/');
    
    return moment(`${parseInt(y, 10) - 543}-${m}-${d}`).format('YYYY-MM-DD');
  }

  onDelete(id: string): void {
    this.appointmentService.delete(id).subscribe(data => {
      console.log(data);

      if (data.status === 1) {
        this.filteredAppointments = this.appointments.filter(app => app.id !== parseInt(id, 10));

        this.toastr.success('ลบข้อมูลเรียบร้อย!!', 'ผลการทำงาน');
      } else {
        this.toastr.error('พบข้อผิดพลาด!!', 'ผลการตรวจสอบ');
      }
    });
  }
}
