import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentPcuListComponent } from './pcu-list.component';

describe('AppointmentPcuListComponent', () => {
  let component: AppointmentPcuListComponent;
  let fixture: ComponentFixture<AppointmentPcuListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppointmentPcuListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentPcuListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
