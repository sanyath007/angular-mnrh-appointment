import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentPostponeComponent } from './postpone.component';

describe('AppointmentPostponeComponent', () => {
  let component: AppointmentPostponeComponent;
  let fixture: ComponentFixture<AppointmentPostponeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppointmentPostponeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentPostponeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
