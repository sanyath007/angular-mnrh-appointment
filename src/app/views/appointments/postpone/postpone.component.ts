import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ModalDismissReasons,
  NgbModal,
  NgbDateStruct,
  NgbInputDatepicker,
} from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { Appointment } from 'src/app/models/Appointment';
import { AppointmentService } from 'src/app/services/appointment/appointment.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-appointment-postpone',
  templateUrl: './postpone.component.html',
  styleUrls: ['./postpone.component.css']
})
export class AppointmentPostponeComponent implements OnInit {
  postponeForm!: FormGroup;
  closeModal!: string;
  submitted = false;
  loading = false;
  currentUser!: any;
  appointType!: number;
  id: string = '';
  

  appointment!: Appointment;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private toastr: ToastrService,
    private appointmentService: AppointmentService,
    private authService: AuthService
  ) {
    this.authService.currentUser.subscribe((x: any) => this.currentUser = x?.user);
  }

  ngOnInit(): void {
    this.route.params.subscribe(param => this.id = param.id);
    /** Get route params and set to appointType */
    this.route.params.subscribe(params => this.appointType = parseInt(params.type));

    this.appointmentService.getById(this.id).subscribe(data => this.appointment = data);

    this.postponeForm = this.fb.group({
      appoint_date: [''],
      appoint_time: [''],
      doctor: ['']
    });
  }

  get f() { return this.postponeForm.controls; }
  get isAdmin() { return this.currentUser.permissions.role.id === 1; }
  get isOfficer() { return this.currentUser.permissions.role.id === 4; }
  get isPcu() { return this.currentUser.permissions.role.id === 5; }

  /** Handling on user select date on calendar of PCU */
  onAppointed(date: Date, modal: any) {
    /** Set date to control input */
    this.postponeForm.controls['appoint_date'].setValue(`${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()+543}`);

    /** ปิดหน้าต่าง Popup */
    modal.close();
  }

  /** Handling on user select date on calendar */
  handleAppointed(data: any, modal: any) {
    console.log(data);
    const { date, period } = data;
    /** Set data's element value to input controls */
    this.postponeForm.controls['appoint_date'].setValue(`${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()+543}`);
    this.postponeForm.controls['appoint_time'].setValue(period);

    /** ปิดหน้าต่าง Popup */
    modal.close();
  }

  triggerModal(content: any) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' })
      .result
      .then(res => {
        this.closeModal = `Close with: ${res}`;
      }, res => {
        this.closeModal = `Dismissed ...`
      });
  }

  onPostpone(e: Event) {
    console.log(this.postponeForm.controls['appoint_date'].value);
    
    this.appointmentService.postpone(
        parseInt(this.id, 10),
        { appoint_date: this.postponeForm.controls['appoint_date'].value }
    ).subscribe(data => {
      if (data.status === 1) {
        this.toastr.success('เลื่อนนัดเรียบร้อย !!', 'ผลการทำงาน');

        if (this.appointType === 1) {
          this.router.navigate(['/appointments-pcu']);
        } else {
          this.router.navigate(['/appointments']);
        }
      } else {
        this.toastr.error('พบข้อมูลผิดพลาด !!', 'ผลการตรวจสอบ');
      }
    });
  }
}
