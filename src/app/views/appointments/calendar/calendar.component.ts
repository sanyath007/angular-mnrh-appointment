import { Component, OnInit } from '@angular/core';
import { AppointmentService } from 'src/app/services/appointment/appointment.service';

@Component({
  selector: 'app-appointment-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class AppointmentCalendarComponent implements OnInit {
  appointType: string = '1';
  cboClinic: string = '10';

  /** Initial data for dropdown controls */
  clinics!: any[];

  constructor(private appointService: AppointmentService) { }

  ngOnInit(): void {
    this.appointService.getInitForm().subscribe(res => {
      this.clinics = res.clinics.filter((clinic: any) => parseInt(clinic.is_special, 10) === 1);
    })
  }

  onAppointed(e: any, modal: any) {

  }
}
