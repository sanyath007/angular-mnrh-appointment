import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  signinForm!: FormGroup;
  loading = false;
  submitted = false;
  returnUrl!: string;
  error = '';

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) {
    /** Redirect to home if already logged in */
    if (this.authService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit(): void {
    this.signinForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });

    /** Get return url from route parameters or default to '/' */
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get username() { return this.signinForm.get('username'); }

  get password() { return this.signinForm.get('password'); }

  onSubmit() {
    this.submitted = true;

    /** Stop here if from is invalid */
    if (this.signinForm.invalid) {
      return;
    }

    const  { username, password } = this.signinForm.value;

    if (username && password) {
      this.loading = true;
      this.authService
        .login(username, password)
        .pipe(first())
        .subscribe(data => {
          location.reload(true);
        }, err => {
          this.error = err;
          this.loading = false;
        });
    }
  }
}
