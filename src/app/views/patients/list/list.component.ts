import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import * as moment from 'moment';
import { PatientService } from 'src/app/services/patient/patient.service';

@Component({
  selector: 'app-patient-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class PatientListComponent implements OnInit {
  patients: any[] = [];
  filteredPatients: any[] = [];
  page = 1;
  pageSize = 10;
  @Output() handleSelected = new EventEmitter();

  filterInputs: any = {
    keyword: '',
    condition: '1'
  };

  constructor(private patientService: PatientService) { }

  ngOnInit(): void {
    this.patientService.getAll().subscribe(data => {
      this.patients = data;
      this.filteredPatients = data;
    })
  }

  calcAge(birthdate: string): number {
    return moment().diff(moment(birthdate), 'year');
  }

  onSelected(e: Event, patient: any) {
    e.preventDefault();

    this.handleSelected.emit(patient);
  }

  onFilter() {
    const { keyword, condition }: { keyword: string, condition: string } = this.filterInputs;

    this.filteredPatients = this.patients.filter(patient => {
      if (keyword !== ''){
        if (condition === '1') {
          return patient.hn.includes(keyword);
        } else if (condition === '2') {
          return patient.cid.includes(keyword);
        } else if (condition === '3') {
          let [fname, lname] = keyword.split(' ');
          
          if (lname !== undefined) {            
            return patient.fname.includes(fname) && patient.lname.includes(lname);
          }

          return patient.fname.includes(fname);
        }
      }

      return patient;
    });
  }

  onClearFilter(): void {
    this.filterInputs.keyword = '';
    this.filterInputs.condition = '1';

    this.filteredPatients = this.patients;
  }
}
