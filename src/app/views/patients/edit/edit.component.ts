import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import {
  NgbModal,
  NgbDateStruct,
  NgbInputDatepicker,
} from '@ng-bootstrap/ng-bootstrap';
import { PatientService } from 'src/app/services/patient/patient.service';
import { ToastrService } from 'ngx-toastr';
import { Formatter } from 'src/app/helpers/formatter';

@Component({
  selector: 'app-patient-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class PatientEditComponent implements OnInit {
  patientForm!: FormGroup;
  submitted: boolean = false;
  closeModal!: string;
  currentUser!: any;
  id: string = '';

  dateModel!: NgbDateStruct;
  @ViewChild('birthDate') birthDate!: NgbInputDatepicker;

  /** Form init */
  changwats!: any[];
  amphurs!: any[];
  filteredAmphurs!: any[];
  tambons!: any[];
  filteredTambons!: any[];
  rights!: any[];
  nationalities!: any[];

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private location: Location,
    private toastr: ToastrService,
    private modalService: NgbModal,
    private formatter: Formatter,
    private patientService: PatientService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(param => this.id = param.id);

    this.patientForm =  this.fb.group({
      id: [''],
      hn: ['', Validators.required],
      cid: ['', Validators.required],
      pname: ['', Validators.required],
      fname: ['', Validators.required],
      lname: ['', Validators.required],
      sex: [''],
      birthdate: ['', Validators.required],
      // address: [''],
      // moo: [''],
      // road: [''],
      // tambon: [''],
      // amphur: [''],
      // changwat: [''],
      // zipcode: [''],
      tel1: ['', Validators.required],
      tel2: [''],
      main_right: ['', Validators.required],
      // hosp_main: [''],
      // hosp_main_desc: [''],
      passport: [''],
      // nationality: [''],
      // race: [''],
      // blood_group: [''],
      remark: [''],
      status: ['']
    });

    this.patientService.getInitForm().subscribe(data => {
      this.changwats = data.changwats;
      this.amphurs = data.amphurs;
      this.tambons = data.tambons;
      this.rights = data.rights;
      this.nationalities = data.nationalities;

      this.filteredAmphurs = this.amphurs.filter(amp => amp.chw_id === '30');
      this.filteredTambons = this.tambons.filter(tam => tam.amp_id === '3001');
    });

    this.patientService.getById(this.id).subscribe(patient => {
      this.patientForm.controls['id'].setValue(patient.id);
      this.patientForm.controls['hn'].setValue(patient.hn);
      this.patientForm.controls['cid'].setValue(patient.cid);
      this.patientForm.controls['pname'].setValue(patient.pname);
      this.patientForm.controls['fname'].setValue(patient.fname);
      this.patientForm.controls['lname'].setValue(patient.lname);
      this.patientForm.controls['sex'].setValue(patient.sex);
      this.patientForm.controls['birthdate'].setValue(this.formatter.dbdateToThdate(patient.birthdate));
      // this.patientForm.controls['address'].setValue(patient.address);
      // this.patientForm.controls['moo'].setValue(patient.moo);
      // this.patientForm.controls['road'].setValue(patient.road);
      // this.patientForm.controls['tambon'].setValue(patient.tambon);
      // this.patientForm.controls['amphur'].setValue(patient.amphur);
      // this.patientForm.controls['changwat'].setValue(patient.changwat);
      // this.patientForm.controls['zipcode'].setValue(patient.zipcode);
      this.patientForm.controls['tel1'].setValue(patient.tel1);
      this.patientForm.controls['tel2'].setValue(patient.tel2);
      this.patientForm.controls['passport'].setValue(patient.passport);
      this.patientForm.controls['main_right'].setValue(patient.main_right);
      // this.patientForm.controls['hosp_main'].setValue(patient.hosp_main);
      // this.patientForm.controls['hosp_main_desc'].setValue(patient.hosp_main_desc);
      // this.patientForm.controls['nationality'].setValue(patient.nationality);
      // this.patientForm.controls['race'].setValue(patient.race);
      // this.patientForm.controls['blood_group'].setValue(patient.blood_group);
      this.patientForm.controls['remark'].setValue(patient.remark);
      this.patientForm.controls['status'].setValue(patient.status);
    })
  }

  get f() { return this.patientForm.controls; }

  onSubmit(): void {
    this.submitted = true;
    
    if (this.patientForm.invalid) {
      return;
    }

    this.patientService.update(parseInt(this.id, 10), this.patientForm.value).subscribe(data => {
      this.toastr.success('แก้ไขข้อมูลเรียบร้อย!!', 'ผลการทำงาน');

      /** Go to previous page */
      this.location.back();
    });
  }

  onChangwatChange(event: Event, selected: string) {
    console.log(selected);
    
    this.filteredAmphurs = this.amphurs.filter(amp => amp.chw_id === selected);
  }
  
  onAmphurChange(event: Event, selected: string) {
    this.filteredTambons = this.tambons.filter(tam => tam.amp_id === selected);
  }

  triggerModal(content: any) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' })
      .result
      .then(res => {
        this.closeModal = `Close with: ${res}`;
      }, res => {
        this.closeModal = `Dismissed ...`
      });
  }
}
