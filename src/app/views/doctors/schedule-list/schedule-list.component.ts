import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { DepartService } from 'src/app/services/depart/depart.service';
import { DoctorService } from 'src/app/services/doctor/doctor.service';
import { ScheduleService } from 'src/app/services/schedule/schedule.service';

@Component({
  selector: 'app-doctor-schedule-list',
  templateUrl: './schedule-list.component.html',
  styleUrls: ['./schedule-list.component.css']
})
export class DoctorScheduleListComponent implements OnInit {
  schedules: any[] = [];
  filteredSchedules: any[] = [];
  page: number = 1;
  pageSize: number = 10;

  doctors: any[] = [];
  filteredDoctors: any[] = [];
  departs: any[] = [];
  cboDoctor: string = '';
  cboDep: string = '';

  constructor(
    private toastr: ToastrService,
    private doctorService: DoctorService,
    private departService: DepartService,
    private scheduleService: ScheduleService,
    ) { }

  ngOnInit(): void {
    this.departService.getAll().subscribe(data => this.departs = data);

    this.doctorService.getAll().subscribe(data => {
      this.doctors = data;
      this.filteredDoctors = data;
    });

    this.scheduleService.getAll().subscribe((data: any) => {
      this.schedules = data;
      this.filteredSchedules = data;
    });
  }

  onDelete(event: Event, id: string) {
    event.preventDefault();

    if (confirm(`คุณต้องการลบรายการตารางออกตรวจ ID : ${id} ใช่หรือไม่?`)) {
      this.scheduleService.delete(id).subscribe(data => {
        if (data.status === 1) {
          this.toastr.success('ลบข้อมูลเรียบร้อย!!', 'ผลการทำงาน');

          /** Remove deleted schedule from filterSchedules property */
          this.filteredSchedules = this.filteredSchedules.filter(sch => sch.id !== id);
        } else {
          this.toastr.error('พบข้อผิดพลาด!!', 'ผลการทำงาน')
        }
      });
    }
  }

  onFilterByDep(dep: string): void {
    /** filtering doctors by selected department */
    this.filteredDoctors = this.doctors.filter(doctor => doctor.depart.id === parseInt(dep, 10));
  }

  onFilterSchedules(doctor: string) {
    /** filtering schedules data by selected doctor */
    this.filteredSchedules = this.schedules.filter(sch => sch.doctor.emp_id == doctor);
  }
}
