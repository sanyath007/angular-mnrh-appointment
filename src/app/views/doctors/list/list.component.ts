import { Component, OnInit } from '@angular/core';
import { Doctor } from 'src/app/models/Doctor';
import { DOCTORS } from 'src/app/mock-data';
import { DoctorService } from 'src/app/services/doctor/doctor.service';
import { DepartService } from 'src/app/services/depart/depart.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-doctor-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class DoctorListComponent implements OnInit {
  doctors: any[] = [];
  filteredDoctors: any[] = [];
  cboDep: string = '';
  departs: any[] = [];
  currentUser!: any;

  page: number = 1;
  pageSize: number = 8;

  constructor(
    private doctorService: DoctorService,
    private departService: DepartService,
    private authService: AuthService
  ) {
    this.authService.currentUser.subscribe(x => this.currentUser = x?.user);
  }

  ngOnInit(): void {
    this.departService.getAll().subscribe(data => this.departs = data);

    this.doctorService.getAll().subscribe(data => {
      this.doctors = data;
      this.filteredDoctors = data;
    });
  }

  onFilterByDep(dep: string): void {
    this.filteredDoctors = this.doctors.filter(doctor => doctor.depart.id === parseInt(dep, 10));
  }
}
