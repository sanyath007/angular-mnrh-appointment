import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  FullCalendarComponent,
  CalendarOptions,
  EventClickArg,
  EventContentArg
} from '@fullcalendar/angular';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DateClickArg } from '@fullcalendar/interaction';
import { DoctorService } from 'src/app/services/doctor/doctor.service';
import { DepartService } from 'src/app/services/depart/depart.service';
import { ScheduleService } from 'src/app/services/schedule/schedule.service';

@Component({
  selector: 'app-doctor-schedule-add',
  templateUrl: './schedule-add.component.html',
  styleUrls: ['./schedule-add.component.css']
})
export class DoctorScheduleAddComponent implements OnInit {
  scheduleForm!: FormGroup;
  submitted = false;
  cboDep: string = '';
  departs: any[] = [];
  doctors: any[] = [];
  filteredDoctors: any[] = [];
  @ViewChild('calendar') calendar!: FullCalendarComponent

  options: CalendarOptions = {
    initialView: 'dayGridMonth',
    height: 'auto',
    headerToolbar: {
      left:   'title',
      center: '',
      right:  ''
    },
    buttonText: {
      today: 'วันนี้'
    },
    locale: 'th',
    displayEventTime: false,
    eventOrder: 'allDay,start',
    dateClick: this.handleDateClick.bind(this)
  };

  events: any[] = [];

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private localeService: BsLocaleService,
    private doctorService: DoctorService,
    private departService: DepartService,
    private scheduleService: ScheduleService
  ) { }

  ngOnInit(): void {
    this.departService.getAll().subscribe(data => this.departs = data);

    this.doctorService.getAll().subscribe(data => {
      this.doctors = data;
      this.filteredDoctors = data;
    });

    this.scheduleForm = this.fb.group({
      depart: [''],
      doctor: ['', Validators.required],
      month: ['', Validators.required],
      period: ['', Validators.required],
      days: ['', Validators.required],
      max_appoint: [10, [Validators.required, Validators.pattern("^[0-9]*$")]]
    });

    this.localeService.use('th-be');
  }

  get f() { return this.scheduleForm.controls; }

  /** This method is created for setting only month seletion */
  onOpenCalendar(container: any) {
    container.monthSelectHandler = (event: any): void => {
      container._store.dispatch(container._actions.select(event.date));

      this.calendar.getApi().gotoDate(event.date)
    };     
    // container.setViewMode('month');
  }

  onSubmit() {
    this.submitted = true;
    
    if (this.scheduleForm.invalid) {
      this.toastr.warning('คุณกรอกข้อมูลไม่ครบ!!', 'ผลการตรวจสอบ');
      return;
    }

    const { month } = this.scheduleForm.value;
    this.scheduleForm.controls['month'].setValue(moment(month).format('YYYY-MM'));
    console.log(this.scheduleForm.value);

    this.scheduleService.store(this.scheduleForm.value).subscribe((data: any) => {
      if (data.status === 1) {
        this.toastr.success('บันทึกข้อมูลเรียบร้อย!!', 'ผลการทำงาน');

        this.router.navigate(['/doctors-schedules'])
      } else {
        this.toastr.error('พบข้อผิดพลาด!!', 'ผลการทำงาน');
      }
    });
  }

  onFilterByDep(dep: string) {
    console.log(dep);
    this.filteredDoctors = this.doctors.filter(doctor => doctor.depart.id === parseInt(dep, 10));    
  }

  isNumberic(obj: any): boolean {
    return Object.keys(obj).length !== 0;
  }

  handleDateClick(arg: DateClickArg) {
    const selectedDate = moment(arg.date).format('YYYY-MM-DD');
    const selectedMonth = moment(arg.date).format('YYYY-MM');
    const scheduleMonth = moment(this.scheduleForm.controls['month'].value).format('YYYY-MM');

    if (scheduleMonth === selectedMonth) {
      if (this.events.find(e => e.start === selectedDate) !== undefined) {
        this.events = this.events.filter(e => e.start !== selectedDate);
        this.options.events = this.events;

        const days = this.scheduleForm.controls['days'].value
                      .split(',')
                      .filter((day: string) => day !== moment(arg.date).format('DD'))
                      .sort()
                      .join();

        this.scheduleForm.controls['days'].setValue(days);
      } else {
        const newEvent = {
          title: '',
          start: selectedDate,
          display: 'background',
          allDay: true,
          backgroundColor: '#8bc24c'
        };

        this.events = [...this.events, newEvent];

        this.options.events = this.events;

        this.scheduleForm.controls['days'].setValue(
          this.events.map(e => moment(e.start).format('DD')).sort().join()
        );
      }
    }
  }

  onClearDays(event: Event): void {
    event.preventDefault();

    this.scheduleForm.controls['days'].setValue('');

    this.events = [];
    this.options.events = [];
  }
}
