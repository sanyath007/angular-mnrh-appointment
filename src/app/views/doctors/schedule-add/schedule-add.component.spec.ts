import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorScheduleAddComponent } from './schedule-add.component';

describe('DoctorScheduleAddComponent', () => {
  let component: DoctorScheduleAddComponent;
  let fixture: ComponentFixture<DoctorScheduleAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorScheduleAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorScheduleAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
