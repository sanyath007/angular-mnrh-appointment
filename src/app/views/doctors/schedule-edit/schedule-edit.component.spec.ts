import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorScheduleEditComponent } from './schedule-edit.component';

describe('DoctorScheduleEditComponent', () => {
  let component: DoctorScheduleEditComponent;
  let fixture: ComponentFixture<DoctorScheduleEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorScheduleEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorScheduleEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
