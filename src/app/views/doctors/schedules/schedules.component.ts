import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { DepartService } from 'src/app/services/depart/depart.service';
import { DoctorService } from 'src/app/services/doctor/doctor.service';

@Component({
  selector: 'app-doctor-schedules',
  templateUrl: './schedules.component.html',
  styleUrls: ['./schedules.component.css']
})
export class DoctorSchedulesComponent implements OnInit {
  calendarEvents!: any[];
  doctors: any[] = [];
  filteredDoctors: any[] = [];
  departs: any[] = [];
  cboDoctor: string = '';
  cboDep: string = '';

  constructor(
    private doctorService: DoctorService,
    private departService: DepartService,
  ) { }

  ngOnInit(): void {
    this.departService.getAll().subscribe(data => this.departs = data);

    this.doctorService.getAll().subscribe(data => {
      this.doctors = data;
      this.filteredDoctors = data;
    });

    this.doctorService.getDoctorSchedules('1').subscribe((data: any[]) => {
      this.calendarEvents = this.createEventFromSchedules(data);
    });
  }

  createEventFromSchedules(schedules: any[]): any[] {
    let events: any[] = [];
    let scheduleDays!: any[];

    schedules.forEach((sch: any) => {
      let periodText = '';
      const days = sch.days.split(',');

      scheduleDays = days.map((d: any) => `${sch.month}-${d}`);
      periodText = sch.period === '1' ? '08.00 - 12.00 น.' : sch.period === '2' ? '13.00 - 16.00 น.' : '16.00 - 20.00 น.'

      let i = 0;

      do {
        let d = moment(scheduleDays[i]);
        i++;

        events.push({
          title: periodText,
          start: d.format('YYYY-MM-DD'),
          // display: 'list-item',
          allDay: true,
          backgroundColor: sch.period === '1' ? '#8bc24c' : sch.period === '2' ? '#5bd1d7' : '#f5587b'
        });
      } while(i < scheduleDays.length);
    });

    return events;
  }

  onFilterByDep(dep: string): void {
    this.filteredDoctors = this.doctors.filter(doctor => doctor.depart.id === parseInt(dep, 10));
  }

  onRederSchedules(doctor: string): void {
    this.doctorService.getDoctorSchedules(doctor).subscribe((data: any[]) => {      
      this.calendarEvents = this.createEventFromSchedules(data);
    });
  }
}
