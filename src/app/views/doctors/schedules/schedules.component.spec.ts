import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorSchedulesComponent } from './schedules.component';

describe('DoctorSchedulesComponent', () => {
  let component: DoctorSchedulesComponent;
  let fixture: ComponentFixture<DoctorSchedulesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorSchedulesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorSchedulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
