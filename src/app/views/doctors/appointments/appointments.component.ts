import { Component, OnInit } from '@angular/core';
import { CalendarEvent } from 'angular-calendar';
import { Department } from 'src/app/models/Department';
import { Doctor } from 'src/app/models/Doctor';
import { DEPARTMENTS, DOCTORS, EVENTS } from 'src/app/mock-data';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-doctor-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.css']
})
export class DoctorAppointmentsComponent implements OnInit {
  events: CalendarEvent[] = [];
  departments: Department[] = [];
  doctors: Doctor[] = [];
  currentUser!: any;

  constructor(private authService: AuthService) {
    this.authService.currentUser.subscribe(x => this.currentUser = x?.user);
  }

  ngOnInit(): void {
    this.events = EVENTS;
    this.departments = DEPARTMENTS;
    this.doctors = DOCTORS;
  }

}
