import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {
  NgbModal,
  NgbDateStruct,
  NgbInputDatepicker,
} from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { DoctorService } from 'src/app/services/doctor/doctor.service';

@Component({
  selector: 'app-doctor-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class DoctorEditComponent implements OnInit {
  doctorForm!: FormGroup;
  closeModal!: string;
  startDate!: NgbDateStruct;
  @ViewChild('birthDate') birthDate!: NgbInputDatepicker;
  submitted: boolean = false;
  currentUser!: any;

  positions!: any[];
  positionClasses!: any[];
  positionTypes!: any[];
  departs!: any[];
  specialists!: any[];
  tmpSpecialists!: any[];

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private doctorService: DoctorService
  ) { }

  ngOnInit(): void {
    let id: string = '';
    this.route.params.subscribe(param => id = param.id);

    this.doctorForm = this.fb.group({
      id: [''],
      cid: ['', Validators.required],
      prefix: ['', Validators.required],
      fname: ['', Validators.required],
      lname: ['', Validators.required],
      sex: [''],
      birthdate: [this.startDate],
      tel1: [''],
      tel2: [''],
      patient_hn: [''],
      title: [''],
      position: ['', Validators.required],
      position_class: ['', Validators.required],
      position_type: ['', Validators.required],
      start_date: [this.startDate],
      license_no: [''],
      license_renewal_date: [this.startDate],
      depart: ['', Validators.required],
      specialist: ['', Validators.required],
      remark: ['']
    });

    this.doctorService.getInitForm().subscribe(data => {
      this.positions = data.positions;
      this.positionClasses = data.positionClasses;
      this.positionTypes = data.positionTypes;
      this.departs = data.departs;
      this.specialists = data.specialists;
    });

    this.doctorService.getById(id).subscribe(doctor => {
      this.doctorForm.controls['id'].setValue(doctor.employee.id);
      this.doctorForm.controls['cid'].setValue(doctor.employee.cid);
      this.doctorForm.controls['prefix'].setValue(doctor.employee.prefix);
      this.doctorForm.controls['fname'].setValue(doctor.employee.fname);
      this.doctorForm.controls['lname'].setValue(doctor.employee.lname);
      this.doctorForm.controls['sex'].setValue(doctor.employee.sex);
      this.doctorForm.controls['birthdate'].setValue(moment(doctor.employee.birthdate).format('DD/MM/YYYY'));
      this.doctorForm.controls['tel1'].setValue(doctor.employee.tel1);
      this.doctorForm.controls['tel2'].setValue(doctor.employee.tel2);
      this.doctorForm.controls['patient_hn'].setValue(doctor.employee.patient_hn);
      this.doctorForm.controls['position'].setValue(doctor.employee.position.id);
      this.doctorForm.controls['position_class'].setValue(doctor.employee.position_class.id);
      this.doctorForm.controls['position_type'].setValue(doctor.employee.position_type.id);
      this.doctorForm.controls['start_date'].setValue(moment(doctor.employee.start_date).format('DD/MM/YYYY'));
      this.doctorForm.controls['title'].setValue(doctor.title);
      this.doctorForm.controls['license_no'].setValue(doctor.license_no);
      this.doctorForm.controls['license_renewal_date'].setValue(moment(doctor.license_renewal_date).format('DD/MM/YYYY'));
      this.doctorForm.controls['depart'].setValue(doctor.depart.id);
      this.doctorForm.controls['specialist'].setValue(doctor.specialists[0].specialist.id);
      this.doctorForm.controls['remark'].setValue(doctor.remark);
    });
  }

  get f() { return this.doctorForm.controls; }

  onSubmit() {
    this.submitted = true;
    console.log(this.doctorForm.value);
    

    if (this.doctorForm.invalid) return;

    // this.doctorService.store(this.doctorForm.value).subscribe(data => {
    //   console.log('Insertion successful!!');
    // });
  }

  onPrefixSelected(prefix: string) {
    if (prefix === 'นาย') {
      this.doctorForm.controls['title'].setValue('นพ.');
      this.doctorForm.controls['sex'].setValue('1');
    } else {
      this.doctorForm.controls['title'].setValue('พญ.');
      this.doctorForm.controls['sex'].setValue('2');
    }
  }

  onDepartSelected(depart: string) {
    this.specialists = this.tmpSpecialists.filter(sp => sp.depart_id === depart);
  }

  triggerModal(content: any) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' })
      .result
      .then(res => {
        this.closeModal = `Close with: ${res}`;
      }, res => {
        this.closeModal = `Dismissed ...`
      });
  }
}
