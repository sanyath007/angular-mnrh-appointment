import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  ModalDismissReasons,
  NgbModal,
  NgbDateStruct,
  NgbInputDatepicker,
} from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { DoctorService } from 'src/app/services/doctor/doctor.service';

@Component({
  selector: 'app-doctor-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class DoctorAddComponent implements OnInit {
  doctorForm!: FormGroup;
  closeModal!: string;
  startDate!: NgbDateStruct;
  @ViewChild('birthDate') birthDate!: NgbInputDatepicker;
  submitted: boolean = false;
  currentUser!: any;

  positions!: any[];
  positionClasses!: any[];
  positionTypes!: any[];
  departs!: any[];
  specialists!: any[];
  tmpSpecialists!: any[];

  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    private toastr: ToastrService,
    private doctorService: DoctorService
  ) { }

  ngOnInit(): void {
    this.doctorForm = this.fb.group({
      cid: [''],
      prefix: ['', Validators.required],
      fname: ['', Validators.required],
      lname: ['', Validators.required],
      sex: [''],
      birthdate: [this.startDate],
      tel1: [''],
      tel2: [''],
      patient_hn: [''],
      title: [''],
      position: ['', Validators.required],
      position_class: ['', Validators.required],
      position_type: ['', Validators.required],
      start_date: [this.startDate],
      license_no: [''],
      license_renewal_date: [this.startDate],
      depart: ['', Validators.required],
      specialist: ['', Validators.required],
      remark: ['']
    });

    this.doctorService.getInitForm().subscribe(data => {
      this.positions = data.positions;
      this.positionClasses = data.positionClasses;
      this.positionTypes = data.positionTypes;
      this.departs = data.departs;
      this.tmpSpecialists = data.specialists;
    })
  }

  get f() { return this.doctorForm.controls; }

  onSubmit() {
    this.submitted = true;
    console.log(this.doctorForm.value);
    

    if (this.doctorForm.invalid) {
      this.toastr.warning('คุณกรอกข้อมูลไม่ครบ!!', 'ผลการตรวจสอบ');
      return;
    }

    this.doctorService.store(this.doctorForm.value).subscribe(data => {
      this.toastr.success('บันทึกข้อมูลเรียบร้อย!!', 'ผลการทำงาน')
    });
  }

  onPrefixSelected(prefix: string) {
    if (prefix === 'นาย') {
      this.doctorForm.controls['title'].setValue('นพ.');
      this.doctorForm.controls['sex'].setValue('1');
    } else {
      this.doctorForm.controls['title'].setValue('พญ.');
      this.doctorForm.controls['sex'].setValue('2');
    }
  }

  onDepartSelected(depart: string) {
    this.specialists = this.tmpSpecialists.filter(sp => sp.depart_id === depart);
  }

  triggerModal(content: any) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' })
      .result
      .then(res => {
        this.closeModal = `Close with: ${res}`;
      }, res => {
        this.closeModal = `Dismissed ...`
      });
  }
}
