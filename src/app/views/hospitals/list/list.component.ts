import { Component, OnInit } from '@angular/core';
import { HospitalService } from 'src/app/services/hospital/hospital.service';

@Component({
  selector: 'app-hospital-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class HospitalListComponent implements OnInit {

  hospitals!: any[];
  filteredHospitals!: any[];
  page: number = 1;
  pageSize: number = 10;
  filterInputs = {
    keyword: '',
    condition: '1',
    changwat: '30'
  };

  constructor(
    private hospitalService: HospitalService
  ) { }

  ngOnInit(): void {
    this.hospitalService.getAll().subscribe(res => {
      this.hospitals = res;
      this.filteredHospitals = res;
    })
  }

  onDelete(e: Event, id: string): void {
    console.log(e, id);
  }

  onFilter(): void {
    this.filteredHospitals = this.hospitals.filter(hospital => {
      const { keyword, condition } = this.filterInputs;
      if (keyword !== '') {
        console.log(condition);
        if (condition === '1') {
          return hospital.hospcode.includes(keyword);
        } else {
          return hospital.name.includes(keyword);
        }
      }

      return hospital;
    });
  }

  onClearFilter(): void {
    this.filterInputs.keyword = '';
    this.filterInputs.condition = '1';
    this.filterInputs.changwat = '30';

    this.filteredHospitals = this.hospitals;
  }

  getHospitalType(type_id: string): string {
    const hosp_types = [
      { id: 1, name: 'สสจ.'},
      { id: 2, name: 'สสอ.'},
      { id: 3, name: 'รพ.สต.'},
      { id: 5, name: 'รพศ.'},
      { id: 7, name: 'รพท.'},
      { id: 9, name: 'ศพช.'},
      { id: 11, name: 'รพ.รัฐสังกัด สธ'},
      { id: 12, name: 'รพ.รัฐนอก สธ'},
      { id: 15, name: 'รพ.เอกชน'},
    ];

    return hosp_types.find(ht => ht.id === parseInt(type_id, 10))?.name!;
  }
}
