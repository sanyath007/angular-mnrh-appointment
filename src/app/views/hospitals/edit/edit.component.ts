import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HospitalService } from 'src/app/services/hospital/hospital.service';

@Component({
  selector: 'app-hospital-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class HospitalEditComponent implements OnInit {

  id: string = '';
  hospital!: any;
  hospitalForm!: FormGroup;
  submitted: boolean = false;

  changwats!: any[];
  amphurs!: any[];
  tambons!: any[];
  filteredAmphurs!: any[];
  filteredTambons!: any[];
  hosp_types = [
    { id: 1, name: 'สสจ.'},
    { id: 2, name: 'สสอ.'},
    { id: 3, name: 'รพ.สต.'},
    { id: 5, name: 'รพศ.'},
    { id: 7, name: 'รพท.'},
    { id: 9, name: 'ศพช.'},
    { id: 11, name: 'รพ.รัฐสังกัด สธ'},
    { id: 12, name: 'รพ.รัฐนอก สธ'},
    { id: 15, name: 'รพ.เอกชน'},
  ];

  areas = [
    { id: '09', name: 'เขตบริการสุขภาพที่ 9' }
  ]


  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private hospitalService: HospitalService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(param => this.id = param.id);

    this.hospitalService.getInitForm().subscribe(res => {
      this.tambons = res.tambons;
      this.amphurs = res.amphurs;
      this.changwats = res.changwats;

      this.filteredTambons = res.tambons;
      this.filteredAmphurs = res.amphurs;
    });

    this.hospitalForm = this.fb.group({
      hospcode: ['', [Validators.required]],
      name: ['', [Validators.required]],
      addrpart: [''],
      moopart: [''],
      tmbpart: ['', [Validators.required]],
      amppart: ['', [Validators.required]],
      chwpart: ['', [Validators.required]],
      hospital_type_id: ['', [Validators.required]],
      hospital_phone: [''],
      hospital_fax: [''],
      area_code: ['', [Validators.required]],
      province_name: ['']
    });

    this.hospitalService.getById(this.id).subscribe(res => {
      this.hospital = res;

      this.hospitalForm.controls['hospcode'].setValue(res.hospcode);
      this.hospitalForm.controls['name'].setValue(res.name);
      this.hospitalForm.controls['addrpart'].setValue(res.addrpart);
      this.hospitalForm.controls['moopart'].setValue(res.moopart);
      this.hospitalForm.controls['chwpart'].setValue(res.chwpart);
      this.hospitalForm.controls['amppart'].setValue(res.amppart);
      this.hospitalForm.controls['tmbpart'].setValue(res.tmbpart);
      this.hospitalForm.controls['hospital_type_id'].setValue(res.hospital_type_id);
      this.hospitalForm.controls['hospital_phone'].setValue(res.hospital_phone);
      this.hospitalForm.controls['hospital_fax'].setValue(res.hospital_fax);
      this.hospitalForm.controls['area_code'].setValue(res.area_code);
      this.hospitalForm.controls['province_name'].setValue(res.province_name);
    })
  }

  get f() { return this.hospitalForm.controls }

  onSubmit() {
    this.submitted = true;

    if (this.hospitalForm.invalid) {
      this.toastr.error("คุณกรอกข้อมูลไม่ครบ!!", "ผลการตรวจสอบ");
      return;
    }

    this.hospitalService.update(this.id, this.hospitalForm.value).subscribe(res => {
      if (res.status === 1) {
        this.toastr.success("แก้ไขข้อมูลเรียบร้อยแล้ว!!", "ผลการทำงาน");

        this.router.navigate(['/hospitals']);
      } else {
        this.toastr.error("พบข้อผิดพลาด ไม่สามารถแก้ไขข้อมูลได้!!", "ผลการทำงาน");
      }
    });
  }

  onChangwatChange(e: Event): void {
    this.filteredAmphurs = this.amphurs.filter(amp => amp.chw_id == (e.target as HTMLInputElement).value);
    this.filteredTambons = this.tambons.filter(tam => tam.chw_id == (e.target as HTMLInputElement).value);

    this.hospitalForm.controls['amppart'].setValue('');
    this.hospitalForm.controls['tmbpart'].setValue('');
  }

  onAmphurChange(e: Event, changwat: string): void {
    this.filteredTambons = this.tambons.filter(tam => tam.amp_id == `${changwat}${(e.target as HTMLInputElement).value}`);

    this.hospitalForm.controls['tmbpart'].setValue('');
  } 
}
