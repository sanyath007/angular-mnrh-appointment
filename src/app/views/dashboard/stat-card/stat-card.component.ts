import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-stat-card',
  templateUrl: './stat-card.component.html',
  styleUrls: ['./stat-card.component.css']
})
export class StatCardComponent implements OnInit {
  @Input() value!: number;
  @Input() title!: string;
  @Input() icon!: string;
  @Input() color!: string;
  @Input() link!: string;

  constructor() { }

  ngOnInit(): void {
  }

}
