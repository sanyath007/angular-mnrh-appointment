import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {
  NgbModal,
  NgbDateStruct,
  NgbInputDatepicker
} from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { DashboardService } from 'src/app/services/dashboard/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  cardStats: any[] = [];
  selectedMonth: string = '';
  startDate!: NgbDateStruct;
  frmMonth!: FormGroup

  // @ViewChild('datePicker') datePicker!: NgbInputDatepicker;

  constructor(private fb: FormBuilder, private dashService: DashboardService) { }

  ngOnInit(): void {
    // this.frmMonth = this.fb.group({ selected: { year: 2024, month: 7 } });

    this.fetchData(moment().format('YYYY-MM'));
  }

  fetchData(_month: string): void {
    this.dashService.getStatCard(_month).subscribe(data => {
      this.cardStats = [
        { title: 'ยอดนัดประจำเดือน', value: data.count.totalcase, color: 'bg-info', icon: 'ion-bag' },
        { title: 'ผู้ป่วยใหม่', value: data.count.newcase, color: 'bg-warning', icon: 'ion-person-add' },
        { title: 'ยอดนัดสูงสุดต่อวัน', value: data.max ? data.max.amt : 0, color: 'bg-success', icon: 'ion-stats-bars' },
        { title: '-', value: '0', color: 'bg-danger', icon: 'ion-pie-graph' },
      ];
    });
  }

  setSelectedMonth($event: string): void {
    this.selectedMonth = $event;

    this.fetchData($event);
  }
}
