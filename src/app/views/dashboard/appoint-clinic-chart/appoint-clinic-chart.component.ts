import { Component, Input, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import * as moment from 'moment';
import { ChartSeries } from 'src/app/helpers/chart-series';
import { DashboardService } from 'src/app/services/dashboard/dashboard.service';

@Component({
  selector: 'app-appoint-clinic-chart',
  templateUrl: './appoint-clinic-chart.component.html',
  styleUrls: ['./appoint-clinic-chart.component.css']
})
export class AppointClinicChartComponent implements OnInit {
  chart!: Chart;
  @Input() month!: string;

  constructor(
    private dashService: DashboardService,
    private chartSeries: ChartSeries
  ) { }

  ngOnInit(): void {
  }

  fetchData(month: string): void {
    this.dashService.getAppointByClinic(month).subscribe(data => {
      let dataSeries: any[] = [];
      data.forEach((d: any) => {        
          dataSeries.push({name: d.clinic_name, y: parseInt(d.amt)});
      });

      this.chart = new Chart({
        chart: {
          type: 'pie'
        },
        title: {
          text: 'สัดส่วนการนัดหมาย จำแนกตามคลินิก'
        },
        accessibility: {
          point: {
              valueSuffix: '%'
          }
        },
        plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %'
              }
          }
        },
        series: [
          {
            type: 'pie',
            name: 'คลินิก',
            colorByPoint: true,
            data: dataSeries
          }
        ]
      });
    });
  }

  ngOnChanges(): void {
    this.fetchData(this.month || moment().format('YYYY-MM'));
  }
}
