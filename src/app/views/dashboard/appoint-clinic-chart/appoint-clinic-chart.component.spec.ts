import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointClinicChartComponent } from './appoint-clinic-chart.component';

describe('AppointClinicChartComponent', () => {
  let component: AppointClinicChartComponent;
  let fixture: ComponentFixture<AppointClinicChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppointClinicChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointClinicChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
