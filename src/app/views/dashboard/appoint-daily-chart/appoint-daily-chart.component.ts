import { Component, Input, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import * as moment from 'moment';
import { ChartSeries } from 'src/app/helpers/chart-series';
import { DashboardService } from 'src/app/services/dashboard/dashboard.service';

@Component({
  selector: 'app-appoint-daily-chart',
  templateUrl: './appoint-daily-chart.component.html',
  styleUrls: ['./appoint-daily-chart.component.css']
})
export class AppointDailyChartComponent implements OnInit {
  chart!: Chart;
  @Input() month!: string;

  constructor(
    private dashService: DashboardService,
    private chartSeries: ChartSeries
  ) { }

  ngOnInit(): void {
  }

  fetchData(month: string): void {
    this.dashService.getAppointPerDay(month).subscribe(data => {
      /** Basic chart */
      let { dataSeries, categories }: { dataSeries: any[], categories: string[] } = this.chartSeries.createDatetimeDataSeries(
        data,
        { name: 'd', value: 'amt'},
        { name: 'm', value: month }
      );
      /** Stack chart */
      // let { dataSeries, categories }: { dataSeries: any[], categories: number[] } = this.chartSeries.createStackedDataSeries(
      //   [ 
      //       { name: 'ER', prop: 'ER' },
      //       { name: 'OPD', prop: 'OPD' },
      //       { name: 'IPD', prop: 'IPD' }
      //   ],
      //   data,
      //   { name: 'd'}
      // );
      this.chart = new Chart({
        chart: {
          type: 'column'
        },
        title: {
          text: 'จำนวนการนัดหมาย (ราย)'
        },
        xAxis: {
          categories: categories
        },
        yAxis: {
          title: {
            text: 'จำนวน (ราย)'
          }
        },
        credits: {
          enabled: false
        },
        series: [{
            name: 'นัดหมายทั้งหมด',
            type: 'column',
            data: dataSeries
          }]
      });
    });
  }

  ngOnChanges(): void {
    this.fetchData(this.month || moment().format('YYYY-MM'));
  }
}
