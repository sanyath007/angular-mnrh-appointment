import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointDailyChartComponent } from './appoint-daily-chart.component';

describe('AppointDailyChartComponent', () => {
  let component: AppointDailyChartComponent;
  let fixture: ComponentFixture<AppointDailyChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppointDailyChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointDailyChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
