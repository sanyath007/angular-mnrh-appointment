import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/services/user/user.service';
import { CustomValidators } from 'src/app/helpers/custom-validators';

@Component({
  selector: 'app-user-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class UserAddComponent implements OnInit {
  userForm!: FormGroup;
  submitted: boolean = false;
  closeModal!: string;
  roles: any[] = [
    { id: 1, name: 'Admin' },
    { id: 2, name: 'Refer Center' },
    { id: 3, name: 'Doctor' },
    { id: 4, name: 'Officer' },
    { id: 5, name: 'PCU' },
    { id: 6, name: 'User' },
    { id: 7, name: 'Guest' },
  ];

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private modalService: NgbModal,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.userForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirm_password: ['', Validators.required],
      fullname: ['', Validators.required],
      email: ['', Validators.email],
      emp_id: [''],
      hospcode: ['', Validators.required],
      hosp_desc: [''],
      position: ['', Validators.required],
      avatar: [''],
      avatarSource: ['']
    }, { validators: CustomValidators.match("password", "confirm_password") });
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.userForm.invalid) {
      this.toastr.error("กรุณากรอกข้อมูลให้ครบ!!", "ผลการตรวจสอบ");
      return;
    }

    // let frmData = new FormData();
    // frmData.append('files', this.userForm.get('avatarSource')?.value);
    // frmData.append('fullname', this.userForm.get('fullname')?.value);

    this.userService.store(this.userForm.value).subscribe((data: any) => {
      console.log(data);
      if (data.status === 1) {
        this.toastr.success("บันทึกข้อมูลเรียบร้อย!!", "ผลการทำงาน");
      } else {
        this.toastr.error("พบข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้!!", "ผลการตรวจสอบ");
      }
    });
  }

  onFileChange(e: Event) {
    if ((e.target as HTMLInputElement).files && (e.target as HTMLInputElement).files?.length) {
      const files = (e.target as HTMLInputElement).files;

      this.userForm.controls['avatarSource'].setValue(files![0]);
    }
  }

  get f() { return this.userForm.controls }
  get passwordMatchError() {
    return (
      this.userForm.getError('mismatch') &&
      this.userForm.get('confirm_password')?.touched
    );
  }

  triggerModal(content: any) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' })
      .result
      .then(res => {
        this.closeModal = `Close with: ${res}`;
      }, res => {
        this.closeModal = `Dismissed ...`
      });
  }

  onHospcodeSelected(hosp: any, modal: any) {
    this.userForm.controls['hospcode'].setValue(hosp.hospcode);
    this.userForm.controls['hosp_desc'].setValue(hosp.name);

    /** ปิดหน้าต่าง Popup */
    modal.close();
  }
}
