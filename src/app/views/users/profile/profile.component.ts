import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class UserProfileComponent implements OnInit {
  currentUser!: any;
  profileForm!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService
  ) {
    this.authService.currentUser.subscribe(x => this.currentUser = x?.user);
  }

  ngOnInit(): void {
    this.profileForm = this.fb.group({
      fullname: [this.currentUser ? this.currentUser.fullname : ''],
      email: [this.currentUser ? this.currentUser.email : ''],
      username: [this.currentUser ? this.currentUser.username : ''],
      position: [this.currentUser ? this.currentUser.position : ''],
      hospcode: [this.currentUser ? this.currentUser.hospcode : ''],
      password: [''],
      confirm_password: [''],
      emp_id: [this.currentUser ? this.currentUser.emp_id : '']
    });
  }

  get f() { return this.profileForm.controls; }
}
