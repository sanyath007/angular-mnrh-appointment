import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class UserListComponent implements OnInit {
  users!: User[];
  filteredUsers!: User[];
  page: number = 1;
  pageSize: number = 10;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getAll().subscribe((data: User[]) => {
      this.users = data;
      this.filteredUsers = data;
    });
  }

  onDelete(event: Event, id: number) {
    event.preventDefault();

    console.log('id : ' + id);
    
  }
}
