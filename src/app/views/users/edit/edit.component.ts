import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class UserEditComponent implements OnInit {
  submitted: boolean = false;
  closeModal!: string;
  userForm!: FormGroup;
  id: string = '';
  roles: any[] = [
    { id: 1, name: 'Admin' },
    { id: 2, name: 'Refer Center' },
    { id: 3, name: 'Doctor' },
    { id: 4, name: 'Officer' },
    { id: 5, name: 'PCU' },
    { id: 6, name: 'User' },
    { id: 7, name: 'Guest' },
  ];

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private modalService: NgbModal,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(param => this.id = param.id);

    this.userForm = this.fb.group({
      id: [''],
      username: [''],
      fullname: [''],
      email: [''],
      hospcode: [''],
      hosp_desc: [''],
      position: [''],
      avatar: [''],
      avatarSource: ['']
    });

    this.userService.getById(this.id).subscribe(res => {
      this.userForm.controls['id'].setValue(res.id);
      this.userForm.controls['username'].setValue(res.username);
      this.userForm.controls['fullname'].setValue(res.fullname);
      this.userForm.controls['email'].setValue(res.email);
      this.userForm.controls['hospcode'].setValue(res.hospcode);
      this.userForm.controls['hosp_desc'].setValue(res.hospital?.name);

      if (res.permissions) {
        this.userForm.controls['position'].setValue(res.permissions[0]?.role?.id);
      }
    });
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.userForm.invalid) {
      this.toastr.error("คุณกรอกข้อมูลไม่ครบ!!", "ผลการตรวจสอบ");
      return;
    }

    /** TODO: If update data along with file upload  */
    // let frmData = new FormData();
    // frmData.append('files', this.userForm.get('avatarSource')?.value);
    // frmData.append('username', this.userForm.get('username')?.value);
    // frmData.append('fullname', this.userForm.get('fullname')?.value);
    // frmData.append('email', this.userForm.get('email')?.value);
    // frmData.append('hospcode', this.userForm.get('hospcode')?.value);
    // frmData.append('position', this.userForm.get('position')?.value);

    this.userService.update(this.id, this.userForm.value).subscribe((res: any) => {
      if (res.status === 1) {
        this.toastr.success("แก้ไขข้อมูลเรียบร้อยแล้ว!!", "ผลการทำงาน");

        this.router.navigate(['/users']);
      } else {
        this.toastr.error("พบข้อผิดพลาด ไม่สามารถแก้ไขข้อมูลได้!!", "ผลการทำงาน");
      }
    });
  }

  onFileChange(e: Event) {
    if ((e.target as HTMLInputElement).files && (e.target as HTMLInputElement).files?.length) {
      const files = (e.target as HTMLInputElement).files;

      this.userForm.controls['avatarSource'].setValue(files![0]);
    }
  }

  get f() { return this.userForm.controls }

  triggerModal(content: any) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' })
      .result
      .then(res => {
        this.closeModal = `Close with: ${res}`;
      }, res => {
        this.closeModal = `Dismissed ...`
      });
  }

  onHospcodeSelected(hosp: any, modal: any) {
    this.userForm.controls['hospcode'].setValue(hosp.hospcode);
    this.userForm.controls['hosp_desc'].setValue(hosp.name);

    /** ปิดหน้าต่าง Popup */
    modal.close();
  }
}
