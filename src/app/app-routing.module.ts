import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { AppointmentListComponent } from './views/appointments/list/list.component';
import { AppointmentExpressListComponent } from './views/appointments/express-list/express-list.component';
import { AppointmentAddComponent } from './views/appointments/add/add.component';
import { DoctorListComponent } from './views/doctors/list/list.component';
import { DoctorDetailComponent } from './views/doctors/detail/detail.component';
import { DoctorAddComponent } from "./views/doctors/add/add.component";
import { DoctorEditComponent } from "./views/doctors/edit/edit.component";
import { DoctorAppointmentsComponent } from './views/doctors/appointments/appointments.component';
import { DoctorSchedulesComponent } from './views/doctors/schedules/schedules.component';
import { DoctorScheduleAddComponent } from './views/doctors/schedule-add/schedule-add.component';
import { DoctorScheduleEditComponent } from './views/doctors/schedule-edit/schedule-edit.component';
import { DoctorScheduleListComponent } from './views/doctors/schedule-list/schedule-list.component';
import { UserProfileComponent } from './views/users/profile/profile.component';
import { SigninComponent } from './views/auth/signin/signin.component';
import { SignupComponent } from './views/auth/signup/signup.component';
import { AppointmentPcuListComponent } from "./views/appointments/pcu-list/pcu-list.component";
import { AppointmentDetailComponent } from "./views/appointments/detail/detail.component";
import { AppointmentEditComponent } from "./views/appointments/edit/edit.component";
import { UserListComponent } from "./views/users/list/list.component";
import { AuthGuard } from "./helpers/auth.guard";
import { PatientAddComponent } from "./views/patients/add/add.component";
import { PatientListComponent } from "./views/patients/list/list.component";
import { PatientEditComponent } from "./views/patients/edit/edit.component";
import { PatientDetailComponent } from "./views/patients/detail/detail.component";
import { AppointmentPostponeComponent } from "./views/appointments/postpone/postpone.component";
import { UserAddComponent } from "./views/users/add/add.component";
import { AppointmentCalendarComponent } from "./views/appointments/calendar/calendar.component";
import { UserEditComponent } from "./views/users/edit/edit.component";
import { HospitalListComponent } from "./views/hospitals/list/list.component";
import { HospitalEditComponent } from "./views/hospitals/edit/edit.component";

const routes: Routes = [
    {
        path: "",
        component: DashboardComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Home',
            breadcrumbs: [
                { label: 'Home', url: '/' }
            ]
        }
    },
    {
        path: "dashboard",
        component: DashboardComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Dashboard',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'Dashboard', url: '/dashboard' }
            ]
        }
    },
    // {
    //     path: "appointments",
    //     component: AppointmentListComponent,
    //     canActivate: [AuthGuard],
    //     data: {
    //         title: 'นัดหมาย - รายวัน',
    //         breadcrumbs: [
    //             { label: 'Home', url: '/' },
    //             { label: 'นัดหมาย - รายวัน', url: '/appointments' }
    //         ]
    //     }
    // },
    // {
    //     path: "appointments-express",
    //     component: AppointmentExpressListComponent,
    //     canActivate: [AuthGuard],
    //     data: {
    //         title: 'นัดหมาย - ด่วนพิเศษ',
    //         breadcrumbs: [
    //             { label: 'Home', url: '/' },
    //             { label: 'นัดหมาย - ด่วนพิเศษ', url: '/appointments-express' }
    //         ]
    //     }
    // },
    {
        path: "appointments",
        component: AppointmentPcuListComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'นัดหมาย - รพ.สต.',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'นัดหมาย - รพ.สต.', url: '/appointments' }
            ]
        }
    },
    {
        path: "appointments/calendar",
        component: AppointmentCalendarComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'ปฏิทินนัดหมาย',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'ปฏิทินนัดหมาย', url: '/appointments/calendar' }
            ]
        }
    },
    {
        path: "appointments/:type/add",
        component: AppointmentAddComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'เพิ่มรายการนัดหมาย',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'นัดหมาย - รายวัน', url: '/appointments' },
                { label: 'เพิ่มรายการนัดหมาย', url: '/appointments/:type/add' }
            ]
        }
    },
    {
        path: "appointments/:id/detail",
        component: AppointmentDetailComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'รายละเอียดการนัดหมาย',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'นัดหมาย - รายวัน', url: '/appointments' },
                { label: 'รายละเอียดการนัดหมาย', url: '/appointments/:id/detail' }
            ]
        }
    },
    {
        path: "appointments/:type/:id/edit",
        component: AppointmentEditComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'แก้ไขการนัดหมาย',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'นัดหมาย - รายวัน', url: '/appointments' },
                { label: 'แก้ไขการนัดหมาย', url: '/appointments/:type/:id/edit' }
            ]
        }
    },
    {
        path: "appointments/:type/:id/postpone",
        component: AppointmentPostponeComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'การเลื่อนนัด',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'นัดหมาย - รายวัน', url: '/appointments' },
                { label: 'การเลื่อนนัด', url: '/appointments/:type/:id/edit' }
            ]
        }
    },
    {
        path: "patients",
        component: PatientListComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'รายชื่อผู้ป่วย',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'รายชื่อผู้ป่วย', url: '/patients' }
            ]
        }
    },
    {
        path: "patients/add",
        component: PatientAddComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'เพิ่มผู้ป่วย',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'รายชื่อผู้ป่วย', url: '/patients' },
                { label: 'เพิ่มผู้ป่วย', url: '/patients/add' }
            ]
        }
    },
    {
        path: "patients/:id/edit",
        component: PatientEditComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'แก้ไขผู้ป่วย',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'รายชื่อผู้ป่วย', url: '/patients' },
                { label: 'แก้ไขผู้ป่วย', url: '/patients/:id/edit' }
            ]
        }
    },
    {
        path: "patients/:id/detail",
        component: PatientDetailComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'รายละเอียดผู้ป่วย',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'รายชื่อผู้ป่วย', url: '/patients' },
                { label: 'รายละเอียดผู้ป่วย', url: '/patients/:id/detail' }
            ]
        }
    },
    {
        path: "doctors",
        component: DoctorListComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'รายชื่อแพทย์',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'รายชื่อแพทย์', url: '/doctors' }
            ]
        }
    },
    {
        path: "doctors/:id/detail",
        component: DoctorDetailComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'ข้อมูลแพทย์',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'รายชื่อแพทย์', url: '/doctors' },
                { label: 'ข้อมูลแพทย์', url: '/doctors/:id/detail' }
            ]
        }
    },
    {
        path: "doctors/add",
        component: DoctorAddComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'เพิ่มแพทย์',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'รายชื่อแพทย์', url: '/doctors' },
                { label: 'เพิ่มแพทย์', url: '/doctors/add' }
            ]
        }
    },
    {
        path: "doctors/:id/edit",
        component: DoctorEditComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'แก้ไขแพทย์',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'รายชื่อแพทย์', url: '/doctors' },
                { label: 'แก้ไขแพทย์', url: '/doctors/:id/edit' }
            ]
        }
    },
    {
        path: "doctors-schedules",
        component: DoctorSchedulesComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'ตารางออกตรวจ',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'รายชื่อแพทย์', url: '/doctors' },
                { label: 'ตารางออกตรวจ', url: '/doctors-schedules' }
            ]
        }
    },
    {
        path: "doctors-schedule/add",
        component: DoctorScheduleAddComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'เพิ่มตารางออกตรวจ',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'รายชื่อแพทย์', url: '/doctors' },
                { label: 'เพิ่มตารางออกตรวจ', url: '/doctors-schedule/add' }
            ]
        }
    },
    {
        path: "doctors-schedule/:id/edit",
        component: DoctorScheduleEditComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'แก้ไขตารางออกตรวจ',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'รายชื่อแพทย์', url: '/doctors' },
                { label: 'แก้ไขตารางออกตรวจ', url: '/doctors-schedule/:id/edit' }
            ]
        }
    },
    {
        path: "doctors-schedule/list",
        component: DoctorScheduleListComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'รายการตารางออกตรวจ',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'รายชื่อแพทย์', url: '/doctors' },
                { label: 'รายการตารางออกตรวจ', url: '/doctors-schedule/list' }
            ]
        }
    },
    {
        path: "doctors-appointments",
        component: DoctorAppointmentsComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'ตารางนัดหมายแพทย์',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'รายชื่อแพทย์', url: '/doctors' },
                { label: 'ตารางนัดหมายแพทย์', url: '/doctors-appointments' }
            ]
        }
    },
    {
        path: "user-profile",
        component: UserProfileComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'User Profile',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'User Profile', url: '/user-profile' }
            ]
        }
    },
    {
        path: "users",
        component: UserListComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'รายการผู้ใช้งาน',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'รายการผู้ใช้งาน', url: '/users' }
            ]
        }
    },
    {
        path: "users/add",
        component: UserAddComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'เพิ่มผู้ใช้งาน',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'รายการผู้ใช้งาน', url: '/users' },
                { label: 'เพิ่มผู้ใช้งาน', url: '/users/add' }
            ]
        }
    },
    {
        path: "users/:id/edit",
        component: UserEditComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'แก้ไขผู้ใช้งาน',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'รายการผู้ใช้งาน', url: '/users' },
                { label: 'แก้ไขผู้ใช้งาน', url: '/users/:id/edit' }
            ]
        }
    },
    {
        path: "hospitals",
        component: HospitalListComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'รายการสถานบริการ',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'รายการสถานบริการ', url: '/hospitals' }
            ]
        }
    },
    {
        path: "hospitals/:id/edit",
        component: HospitalEditComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'แก้ไขสถานบริการ',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'รายการสถานบริการ', url: '/hospitals' },
                { label: 'แก้ไขสถานบริการ', url: '/hospitals/:id/edit' }
            ]
        }
    },
    {
        path: "signin",
        component: SigninComponent,
        data: {
            title: 'ล็อกอิน',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'ล็อกอิน', url: '/signin' }
            ]
        }
    },
    {
        path: "signup",
        component: SignupComponent,
        data: {
            title: 'ลงทะเบียนผู้ใช้',
            breadcrumbs: [
                { label: 'Home', url: '/' },
                { label: 'ลงทะเบียนผู้ใช้', url: '/signup' }
            ]
        }
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {}
