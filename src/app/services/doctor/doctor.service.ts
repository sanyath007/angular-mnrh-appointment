import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<any[]> {
    return this.http.get<any[]>(`${environment.apiUrl}/api/doctors`);
  }

  getById(id: string): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/doctors/${id}`);
  }

  getDortorsOfClinic(clinic: number): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/doctors/${clinic}/clinic`);
  }

  getDoctorSchedules(id: string): Observable<any[]> {
    return this.http.get<any[]>(`${environment.apiUrl}/api/doctors/${id}/schedules`);
  }

  getInitForm(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/doctors/init/form`);
  }

  store(data: any): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}/api/doctors`, data);
  }

  update(id: string, data: any): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}/api/doctors/${id}`, data);
  }

  delete(id: string): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}/api/doctors/${id}`);
  }
}
