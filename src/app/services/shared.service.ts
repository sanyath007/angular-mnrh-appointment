import { Injectable } from '@angular/core';
import { Data } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  routeData!: Data;

  constructor() { }

  setRouteData(data: Data) {
    this.routeData = data;
  }

  getRouteData(): Data {
    return this.routeData;
  }
}
