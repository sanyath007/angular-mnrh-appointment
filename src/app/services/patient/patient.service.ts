import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const httpOptions = {

};

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  constructor(private http: HttpClient) { }

  getInitForm(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/patients/init/form`);
  }

  getAll(): Observable<any[]> {
    return this.http.get<any[]>(`${environment.apiUrl}/api/patients`);
  }

  getById(id: string): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/patients/${id}`);
  }

  getByCid(cid: string): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/patients/${cid}/cid`);
  }

  getByHCid(cid: string): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/hpatients/${cid}/cid`);
  }

  getHByHn(hn: string): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl2}/api/hpatients/${hn}/hn`);
  }

  update(id: number, data: any): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}/api/patients/${id}`, data, httpOptions)
  }

  delete(id: number): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}/api/patients/${id}`)
  }
}
