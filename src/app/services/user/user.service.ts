import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/User';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<User[]> {
    return this.http.get<User[]>(`${environment.apiUrl}/api/users`);
  }

  getById(id: string): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/users/${id}`);
  }

  store(data: any): Observable<any> {
    const httpOptions = {
      // headers: new HttpHeaders({ 'Content-Type': 'multipart/form-data' })
    };

    return this.http.post<any>(`${environment.apiUrl}/api/users`, data, httpOptions);
  }

  update(id: string, data: any): Observable<any> {
    const httpOptions = {
      // headers: new HttpHeaders({ 'Content-Type': 'multipart/form-data' })
    };

    return this.http.put<any>(`${environment.apiUrl}/api/users/${id}`, data, httpOptions);
  }

  delete(id: string): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}/api/users/${id}`);
  }
}
