import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }

  getStatCard(month: string): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/dashboard/${month}/stat-card`);
  }

  getAppointPerDay(month: string): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/dashboard/${month}/appoint-day`);
  }

  getAppointByClinic(month: string): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/dashboard/${month}/appoint-by-clinic`);
  }
}
