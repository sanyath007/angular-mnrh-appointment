import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Appointment } from 'src/app/models/Appointment';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

const httpOptions = {

};

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Appointment[]> {
    return this.http.get<Appointment[]>(`${environment.apiUrl}/api/appointments`);
  }

  getById(id: string): Observable<Appointment> {
    return this.http.get<Appointment>(`${environment.apiUrl}/api/appointments/${id}`);
  }

  getAllByDate(date: string, type: number): Observable<any[]> {
    return this.http.get<any[]>(`${environment.apiUrl}/api/appointments/${date}/${type}/count`);
  }

  getAllByClinic(date: string, type: number, clinic: string): Observable<any[]> {
    return this.http.get<any[]>(`${environment.apiUrl}/api/appointments/${date}/${type}/${clinic}`);
  }

  store(data: Appointment): Observable<Appointment> {
    return this.http.post<Appointment>(`${environment.apiUrl}/api/appointments`, data, httpOptions);
  }

  update(id: string, data: Appointment): Observable<Appointment> {
    return this.http.put<Appointment>(`${environment.apiUrl}/api/appointments/${id}`, data, httpOptions);
  }

  delete(id: string): Observable<Appointment> {
    return this.http.delete<Appointment>(`${environment.apiUrl}/api/appointments/${id}`);
  }

  getInitForm(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/appointments/init/form`);
  }

  complete(id: number, data: any): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}/api/appointments/${id}/complete`, data, httpOptions);
  }

  cancel(id: number, data: any): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}/api/appointments/${id}/cancel`, data, httpOptions);
  }

  postpone(id: number, data: any): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}/api/appointments/${id}/postpone`, data, httpOptions);
  }
}
