import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<any[]> {
    return this.http.get<any[]>(`${environment.apiUrl}/api/schedules`);
  }

  getById(id: string): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/schedules/${id}`);
  }

  getInitForm(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/schedules/init/form`);
  }

  store(data: any): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}/api/schedules`, data);
  }

  update(id: string, data: any): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}/api/schedules/${id}`, data);
  }

  delete(id: string): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}/api/schedules/${id}`);
  }
}
