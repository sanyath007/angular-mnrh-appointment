import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
/** Modules */
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { registerLocaleData, CommonModule } from '@angular/common';
// import { MaterialModule } from './material.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbCalendar, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { InputMaskModule } from '@ngneat/input-mask';
import { ToastrModule } from 'ngx-toastr';
import { ChartModule } from 'angular-highcharts';
import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { thBeLocale } from 'ngx-bootstrap/chronos';
/** App components */
import { AppComponent } from './app.component';
/** Layouts Components */
import { MainFooterComponent } from './layouts/main-footer/main-footer.component';
import { MainHeaderComponent } from './layouts/main-header/main-header.component';
import { MainSidebarComponent } from './layouts/main-sidebar/main-sidebar.component';
import { ContentHeaderComponent } from './layouts/content-header/content-header.component';
import { LoggedinMenuItemComponent } from './layouts/loggedin-menu-item/loggedin-menu-item.component';
/** Common Components */
import { PaginationComponent } from './components/pagination/pagination.component';
import { SelectInputComponent } from './components/forms/select-input/select-input.component';
import { AppointmentsComponent } from './components/appointments/appointments.component';
import { CalendarSelectionComponent } from './components/calendar-selection/calendar-selection.component';
import { CalendarSelectionPcuComponent } from './components/calendar-selection-pcu/calendar-selection-pcu.component';
import { AddEventComponent } from './components/add-event/add-event.component';
import { DoctorCardComponent } from './components/doctor-card/doctor-card.component';
/** Views Components */
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { AppointmentAddComponent } from './views/appointments/add/add.component';
import { AppointmentListComponent } from './views/appointments/list/list.component';
import { AppointmentExpressListComponent } from './views/appointments/express-list/express-list.component';
import { AppointmentPcuListComponent } from './views/appointments/pcu-list/pcu-list.component';
import { AppointmentDetailComponent } from './views/appointments/detail/detail.component';
import { AppointmentEditComponent } from './views/appointments/edit/edit.component';
import { DoctorListComponent } from './views/doctors/list/list.component';
import { DoctorDetailComponent } from './views/doctors/detail/detail.component';
import { DoctorAppointmentsComponent } from './views/doctors/appointments/appointments.component';
import { DoctorAddComponent } from './views/doctors/add/add.component';
import { DoctorEditComponent } from './views/doctors/edit/edit.component';
import { PatientListComponent } from './views/patients/list/list.component';
import { SigninComponent } from './views/auth/signin/signin.component';
import { SignupComponent } from './views/auth/signup/signup.component';
import { UserListComponent } from './views/users/list/list.component';
import { UserProfileComponent } from './views/users/profile/profile.component';
/** Helpers services */
import { JwtInterceptor } from './helpers/jwt.interceptor';
import { ErrorInterceptor } from './helpers/error.interceptor';
import { CustomAdapter } from './helpers/custom-adapter';
import { CustomDateParserFormatter } from './helpers/custom-parser-formatter';
import { CustomDatePickerI18n, I18n } from './helpers/custom-datepicker-i18n';
import {
  NgbDateAdapter,
  NgbDateParserFormatter,
  NgbCalendarBuddhist,
  NgbDatepickerI18n
} from '@ng-bootstrap/ng-bootstrap';
import { ThDatePipe } from './helpers/pipes/thdate';
import { ShortTimePipe } from './helpers/pipes/shorttime';
import localeTh from '@angular/common/locales/th';
import { StatCardComponent } from './views/dashboard/stat-card/stat-card.component';
import { AppointDailyChartComponent } from './views/dashboard/appoint-daily-chart/appoint-daily-chart.component';
import { AppointClinicChartComponent } from './views/dashboard/appoint-clinic-chart/appoint-clinic-chart.component';
import { AppointmentCalendarComponent } from './views/appointments/calendar/calendar.component';
import { DoctorSchedulesComponent } from './views/doctors/schedules/schedules.component';
import { DoctorScheduleComponent } from './components/doctor-schedule/doctor-schedule.component';
import { DoctorScheduleAddComponent } from './views/doctors/schedule-add/schedule-add.component';
import { DoctorScheduleEditComponent } from './views/doctors/schedule-edit/schedule-edit.component';
import { DoctorScheduleListComponent } from './views/doctors/schedule-list/schedule-list.component';
import { PatientAddComponent } from './views/patients/add/add.component';
import { PatientEditComponent } from './views/patients/edit/edit.component';
import { PatientDetailComponent } from './views/patients/detail/detail.component';
import { AppointmentPostponeComponent } from './views/appointments/postpone/postpone.component';
import { Formatter } from './helpers/formatter';
import { UserAddComponent } from './views/users/add/add.component';
import { HospcodeListComponent } from './components/hospital-list/hospcode-list.component';
import { CalendarAppointmentComponent } from './components/calendar-appointment/calendar-appointment.component';
import { UserEditComponent } from './views/users/edit/edit.component';
import { HospitalListComponent } from './views/hospitals/list/list.component';
import { HospitalEditComponent } from './views/hospitals/edit/edit.component';
import { TambonSelectComponent } from './components/forms/tambon-select/tambon-select.component';
import { AmphurSelectComponent } from './components/forms/amphur-select/amphur-select.component';
import { MonthPickerComponent } from './components/forms/month-picker/month-picker.component';

registerLocaleData(localeTh);
FullCalendarModule.registerPlugins([
  dayGridPlugin,
  interactionPlugin
]);

defineLocale('th-be', thBeLocale);

@NgModule({
  declarations: [
    AppComponent,
    MainFooterComponent,
    MainHeaderComponent,
    MainSidebarComponent,
    ContentHeaderComponent,
    DashboardComponent,
    AppointmentsComponent,
    AppointmentAddComponent,
    AppointmentListComponent,
    AppointmentExpressListComponent,
    AppointmentPcuListComponent,
    AppointmentDetailComponent,
    AppointmentEditComponent,
    PaginationComponent,
    DoctorListComponent,
    DoctorDetailComponent,
    DoctorAppointmentsComponent,
    DoctorAddComponent,
    DoctorEditComponent,
    DoctorCardComponent,
    UserProfileComponent,
    CalendarSelectionComponent,
    CalendarSelectionPcuComponent,
    AddEventComponent,
    PatientListComponent,
    SigninComponent,
    SignupComponent,
    LoggedinMenuItemComponent,
    SelectInputComponent,
    UserListComponent,
    ThDatePipe,
    ShortTimePipe,
    StatCardComponent,
    AppointDailyChartComponent,
    AppointClinicChartComponent,
    AppointmentCalendarComponent,
    DoctorSchedulesComponent,
    DoctorScheduleComponent,
    DoctorScheduleAddComponent,
    DoctorScheduleEditComponent,
    DoctorScheduleListComponent,
    PatientAddComponent,
    PatientEditComponent,
    PatientDetailComponent,
    AppointmentPostponeComponent,
    UserAddComponent,
    HospcodeListComponent,
    CalendarAppointmentComponent,
    UserEditComponent,
    HospitalListComponent,
    HospitalEditComponent,
    TambonSelectComponent,
    AmphurSelectComponent,
    MonthPickerComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    // MaterialModule,
    NgbModule,
    InputMaskModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    ToastrModule.forRoot(),
    ChartModule,
    FullCalendarModule,
    BsDatepickerModule.forRoot(),
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    I18n,
    { provide: NgbDateAdapter, useClass: CustomAdapter },
    { provide: NgbDatepickerI18n, useClass: CustomDatePickerI18n },
    { provide: NgbCalendar, useClass: NgbCalendarBuddhist },
    { provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter },
    Formatter,
  ],
  bootstrap: [AppComponent],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule { }
