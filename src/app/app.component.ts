import { Component, OnInit, DoCheck, Renderer2 } from '@angular/core';
import { Data, Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, DoCheck {
  title = 'angular-mnrh-appointment';
  contentHeader!: Data;
  isAuth = false;
  isOnSigninPage = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private renderer: Renderer2
  ) { }

  ngOnInit() { }

  ngDoCheck() {    
    // TODO: should move this to its own services
    /** If route path is signin or signup have to set css class to body tag */
    const currentRoute = this.router.url.split('?')[0];
    if (currentRoute !== '/signin' && currentRoute !== '/signup') {
      this.isAuth = true;

      this.renderer.removeClass(document.body, 'login-page');
      this.renderer.removeClass(document.body, 'register-page');
      this.renderer.addClass(document.body, 'layout-fixed');
    } else {
      this.renderer.removeClass(document.body, 'layout-fixed');
      this.isAuth = false;

      if (currentRoute === '/signin') {
        this.renderer.addClass(document.body, 'login-page');
        this.isOnSigninPage = true;
      } else  if (currentRoute === '/signup') {
        this.renderer.addClass(document.body, 'register-page');
        this.isOnSigninPage = false;
      }
    }
  }
}
