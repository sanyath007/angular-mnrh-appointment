import { Injectable } from "@angular/core";
import * as moment from "moment";

@Injectable({ providedIn: 'root' })
export class ChartSeries {
  constructor() { }

  createDailyCategories(): number[] {
    return new Array(24);
  };

  createMonthlyCategories(month: string): number[] {
    if(!month) return new Array(31)
    
    let endDate = parseInt(moment(`${month}-01`).endOf('month').format('DD'));

    return new Array(endDate);
  }
    
  createYearlyCategories(lang: string): string[] | number[] {
    let months = null;
    
    if(lang === 'en') {
      months = ['Oct', 'Nov', 'Dec', 'Jan', 'Feb', 'Mar', 'Api', 'May', 'Jun', 'Jul', 'Aug', 'Sep'];
    } else {
      months = ['ตค', 'พย', 'ธค', 'มค', 'กพ', 'มีค', 'เมย', 'พค', 'มิย', 'กค', 'สค', 'กย'];
    }

    return months;
  }

  createDatetimeDataSeries(data: any[], dataProps: any, cateType: any): any {
    let dataSeries = [];
    let categories: string[] = [];
    let arrCates: number[] = [];
    let cateVal = 0;

    if(cateType.name == 'd') {
      arrCates = this.createDailyCategories();
    } else if (cateType.name == 'm') {
      arrCates = this.createMonthlyCategories(cateType.value);
    }

    for(let i = 0; i < arrCates.length; i++) {
      if(cateType.name == 'd') {
        cateVal = i;
      } else if (cateType.name == 'm') {
        cateVal = i+1;
      }

      categories[i] = cateVal.toString();
      dataSeries.push(0);

      data.every((val, key) => {
        if(parseInt(val[dataProps.name]) === cateVal) {
          dataSeries[i] = parseInt(val[dataProps.value]);
          return false;
        }

        return true;
      });
    }

    return { dataSeries, categories }
  }

  createSeries(stacked: any[]): any[] {
    let series: any[] = [];

    stacked.forEach((val, key) => {
      series.push({
        name: val.name,
        prop: val.prop,
        color: val.color,
        data: []
      });
    });

    return series;
  }

  createStackedDatetimeDataSeries(stacked: any[], data: any[], dataProps: any, cateType: any) {
    let dataSeries: any[] = [];
    let categories: number[] = [];
    let arrCates: number[] = [];
    let cateVal = '';
    
    dataSeries = this.createSeries(stacked);
    
    if(cateType.name == 'd') {
      arrCates = this.createDailyCategories();
    } else if (cateType.name == 'm') {
      arrCates = this.createMonthlyCategories(cateType.value);
    }

    for(let i = 0; i < arrCates.length; i++) {
      if(cateType.name == 'd') {
        cateVal = i.toString();
      } else if (cateType.name == 'm') {
        cateVal = (i+1).toString();
      }

      data.every((val, key) => {
        if(val[dataProps.name] == cateVal) {
          dataSeries.forEach((s, key) => {
            s.data[i] = parseInt(val[s.prop]);
          });

          return false;
        } else {
          dataSeries.forEach((s, key) => {
            s.data[i] = 0;
          });
        }

        return true;
      });
    }

    return { dataSeries, categories }
  }

  createStackedDataSeries(stacked: any[], data: any[], dataProps: any) {
    let dataSeries: any[] = [];
    let categories: any[] = [];
    let cateVal = '';
    
    dataSeries = this.createSeries(stacked);

    categories = data.map(d => {
      return d[dataProps.name]+ '-' +d.name;
    });

    // for(let i = 0; i < categories.length; i++) {
    //   cateVal = categories[i].toString().substr(0, 2);

    //   data.every((val, key) => {     
    //     console.log(val[dataProps.name]+ '==' +cateVal);

    //     if(val[dataProps.name] == cateVal) {
    //       dataSeries.forEach((s, key) => {
    //         s.data[i] = parseInt(val[s.prop]);
    //       });

    //       return false;
    //     } else {
    //       dataSeries.forEach((s, key) => {
    //         s.data[i] = 0;
    //       });
    //     }

    //     return true;
    //   });
    // }

    return { dataSeries, categories }
  }
}