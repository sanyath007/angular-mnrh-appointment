import { Injectable } from '@angular/core';
import { NgbDateStruct, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';

const I18N_VALUES = {
  'th': {
    weekdays: ['จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส', 'อา'],
    months: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
    weekLabel: ''
  }
};

@Injectable()
export class I18n {
  language: string = 'th';
}

@Injectable()
export class CustomDatePickerI18n extends NgbDatepickerI18n {
  lang = this._i18n.language as keyof typeof I18N_VALUES;

  constructor(private _i18n: I18n) {
    super(); 
    console.log(this._i18n.language);
  }

  getWeekdayShortName(weekday: number): string { throw new Error('Method not implemented.'); }
  getWeekdayLabel(weekday: number): string { return I18N_VALUES[this.lang].weekdays[weekday - 1]; }
  getWeekLabel(): string { return I18N_VALUES[this.lang].weekLabel; }
  getMonthShortName(month: number): string { return I18N_VALUES[this.lang].months[month - 1]; }
  getMonthFullName(month: number): string { return this.getMonthShortName(month); }
  getDayAriaLabel(date: NgbDateStruct): string { return `${date.day}-${date.month}-${date.year}`; }
}
