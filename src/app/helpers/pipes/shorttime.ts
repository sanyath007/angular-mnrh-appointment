import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'shorttime' })
export class ShortTimePipe implements PipeTransform {
    transform(value: string): string {
        let [hh, mm, ss] = value.split(':');
        return `${hh}:${mm}`;
    }
}
