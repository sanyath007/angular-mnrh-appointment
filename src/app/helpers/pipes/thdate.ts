import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'thdate' })
export class ThDatePipe implements PipeTransform {
    transform(value: string): string {
        let [year, month, date] = value.split('-');

        return `${date}/${month}/${parseInt(year, 10) + 543}`;
    }
}
