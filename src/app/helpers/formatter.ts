import { Injectable } from "@angular/core";

@Injectable()
export class Formatter {
    constructor() { }

    dbdateToThdate(date: string): string {
        const [year, month, day] = date.split('-');

        return `${day}/${month}/${parseInt(year, 10) + 543}`;
    }
}