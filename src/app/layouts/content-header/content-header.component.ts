import { Component, OnInit, DoCheck, Input } from '@angular/core';
import { Data } from '@angular/router';

@Component({
  selector: 'app-content-header',
  templateUrl: './content-header.component.html',
  styleUrls: ['./content-header.component.css']
})
export class ContentHeaderComponent implements OnInit, DoCheck {
  @Input() data!: Data;

  constructor() { }

  ngOnInit(): void {
    // console.log('content-header OnInit is occured!!');
  }

  ngDoCheck() {
    // console.log('content-header DoCheck is occured!!');
  }
}
