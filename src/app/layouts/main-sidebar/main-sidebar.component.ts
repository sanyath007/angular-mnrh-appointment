import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-main-sidebar',
  templateUrl: './main-sidebar.component.html',
  styleUrls: ['./main-sidebar.component.css']
})
export class MainSidebarComponent implements OnInit {
  currentUser!: any;

  constructor(private authService: AuthService) {
    this.authService.currentUser.subscribe(x => this.currentUser = x?.user);
  }

  ngOnInit(): void {
  }

  get isAdmin(): boolean {
    if (typeof this.currentUser?.permissions != 'undefined') {
      return  this.currentUser?.permissions[0]?.role?.id === 1;
    };
  
    return false;
  }

  get isPcu(): boolean {
    if (typeof this.currentUser?.permissions != 'undefined') {
      return  this.currentUser?.permissions[0]?.role?.id === 5;
    };

    return false;
  }

  get isHospital(): boolean {
    if (typeof this.currentUser?.permissions != 'undefined') {
      return  this.currentUser?.permissions[0]?.role?.id === 6;
    };

    return false;
  }
}
