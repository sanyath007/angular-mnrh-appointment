import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoggedinMenuItemComponent } from './loggedin-menu-item.component';

describe('LoggedinMenuItemComponent', () => {
  let component: LoggedinMenuItemComponent;
  let fixture: ComponentFixture<LoggedinMenuItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoggedinMenuItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedinMenuItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
