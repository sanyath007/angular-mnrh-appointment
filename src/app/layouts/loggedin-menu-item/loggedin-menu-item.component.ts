import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-loggedin-menu-item',
  templateUrl: './loggedin-menu-item.component.html',
  styleUrls: ['./loggedin-menu-item.component.css']
})
export class LoggedinMenuItemComponent implements OnInit {
  @Input() user!: User;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  get isPcu(): boolean {
    if (typeof this.user?.permissions != 'undefined') {
      return  this.user?.permissions[0]?.role?.id === 5;
    };

    return false;
  }

  onLogout() {
    this.authService.logout();

    location.reload();
  }
}
