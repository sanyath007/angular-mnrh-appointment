// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // apiUrl: 'http://localhost/public_html/slim3-appointment-api/public', /** localhost on win */
  // apiUrl: 'http://localhost:8080/slim3-appointment-api/public', /** localhost on mac */
  apiUrl: 'https://smkorat.com/appointment-api/public', /** URL Host เวชกรรมฯ */
  apiUrl2: 'https://pharm.mnrh.go.th/appointment-api/public',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
